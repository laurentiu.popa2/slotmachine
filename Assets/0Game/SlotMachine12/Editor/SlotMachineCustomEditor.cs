using UnityEngine;
using UnityEditor;
using System.Collections;

[System.Serializable]
[UnityEditor.CustomEditor(typeof(SlotMachines))]
public class SlotMachineCustomEditor : Editor
{
	SlotMachines slotScript;
    public override void OnInspectorGUI()
    {
        this.DrawDefaultInspector();
		slotScript = (SlotMachines)target;
        if (!Application.isPlaying)
        {
            if (GUILayout.Button("Generate A New Line Object", new GUILayoutOption[] {}))
            {
                slotScript.GenerateNewLine();
            }
            if (GUILayout.Button("Reset User Stats", new GUILayoutOption[] {}))
            {
                PlayerPrefs.DeleteAll();
            }
        }
        if (Application.isPlaying)
        {
            GUILayout.BeginHorizontal(new GUILayoutOption[] {});
            if (GUILayout.Button("Force A Bonus", new GUILayoutOption[] {}))
            {
                if (slotScript.spinning)
                {
                    slotScript.tempBonusState = 3;
                }
                else
                {
                    Debug.Log("You can only force a bonus game during a spin");
                }
            }
            if (GUILayout.Button("Force A Scatter", new GUILayoutOption[] {}))
            {
                if (slotScript.spinning)
                {
                    slotScript.tempScatter = 3;
                }
                else
                {
                    Debug.Log("You can only force a scatter during a spin");
                }
            }
            GUILayout.EndHorizontal();
        }
    }

}