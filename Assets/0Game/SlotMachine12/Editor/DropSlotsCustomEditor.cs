using UnityEngine;
using UnityEditor;
using System.Collections;

[System.Serializable]
[UnityEditor.CustomEditor(typeof(DropSlots))]
public class DropSlotsCustomEditor : Editor
{
	DropSlots slotScript;
    public override void OnInspectorGUI()
    {
   
        this.DrawDefaultInspector();
		slotScript = (DropSlots)target;
        if (!Application.isPlaying)
        {
            if (GUILayout.Button("Generate A New Line Object", new GUILayoutOption[] {}))
            {
                slotScript.GenerateNewLine();
            }
            if (GUILayout.Button("Reset User Stats", new GUILayoutOption[] {}))
            {
                PlayerPrefs.DeleteKey("Coins");
                PlayerPrefs.DeleteKey("Level");
                PlayerPrefs.DeleteKey("Experience");
                PlayerPrefs.DeleteKey("LastLevelExperience");
                PlayerPrefs.DeleteKey("ExperienceToLevel");
            }
            GUILayout.BeginHorizontal(new GUILayoutOption[] {});
			if (GUILayout.Button("More Awesome Project", new GUILayoutOption[] {}))
            {
                Application.OpenURL("");
                Debug.Log("Click My Shop");
            }
            if (GUILayout.Button("Rate this project", new GUILayoutOption[] {}))
            {
                Application.OpenURL("");
                Debug.Log("Click Rate Project");
            }
            GUILayout.EndHorizontal();
        }
        if (Application.isPlaying)
        {
            GUILayout.BeginHorizontal(new GUILayoutOption[] {});
            if (GUILayout.Button("Force A Scatter", new GUILayoutOption[] {}))
            {
                if (slotScript.dropping)
                {
                    slotScript.tempScatter = 3;
                }
                else
                {
                    Debug.Log("You can only force a scatter during a spin");
                }
            }
            GUILayout.EndHorizontal();
        }
    }

}