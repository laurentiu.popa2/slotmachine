using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public partial class DropSlots : MonoBehaviour
{
    public GameObject SpinObj;
    public GameObject AutoSpinObj;
    public GameObject StopAutoObj;
    public GameObject AutoSpinEffectObj;
    public GameObject TotalBetObj;
    public GameObject BetLineObj;
    public GameObject LineObj;
    public GameObject LobbyObj;
    public GameObject PayTableObj;
    public GameObject CoinPanel;
    public GameObject Header;
    public Animator PayTableAnimator;
    public Animator LoadingAnimator;
    public GameObject SlotCam;
    public GameObject AutoSpinEffect;
    public GameObject Auto;
    public GameObject Stop;
    public TextMesh LastWin;
    public TextMesh CoinsTxt;
    public ParticleSystem coinShower;
    public ParticleSystem explosionParticle;
    public SpriteRenderer[] trapDoors;
    public Vector2[] elbowPositions;
    public GameObject scatterObject;
    public TextMesh scatterCountObject;
    public GameObject payTableImage;
    public PayoutOrder payoutOrder;
    [UnityEngine.Range(0, 5)]
    public float iconSize;
    [UnityEngine.Range(0, 20)]
    public int scatterSize;
    [UnityEngine.Range(3, 25)]
    public int maxBet;
    public float[] betAmounts;
    public IconInfo[] iconInfo;
    //public AudioInfo[] audioInfo;
    public LinesInfo linesInfo;
    public ButtonInfo[] buttonInfo;
    public DisplayInfo[] displayInfo;
    public GameObject lines;
    public ReelInfo[] reelInfo;
    [UnityEngine.HideInInspector]
    public int tempScatter;
    [UnityEngine.HideInInspector]
    public bool spinning;
    [UnityEngine.HideInInspector]
    public bool dropping;
    private GameObject currentSymbolBundle;
    private bool displayWinningEffects;
    private bool generatingSymbols;
    private bool generatingLine;
    private bool showPayTable;
    private bool touching;
    private int scattersLeft;
    private int lineCount;
    private float scatterTimer;
    private float autoSpinTimer;
    private float effectsTimer;
    private float totalPayout;
    private float fadeValue;
    private int[] faceIcons;
    private int currentBet;
    private int picks;
    private Vector3[] linePositions;
    private bool isAutoSpin = false;
    private bool isAddCoins = false;
    public virtual void Awake()
    {
        SlotCam.SetActive(true);
        isAddCoins = false;
        CoinsTxt.text = DataManager.Instance.FluxCoins.ToString("n0");
        this.generatingSymbols = true;
        System.Array.Resize<int>(ref this.faceIcons, 15);
		System.Array.Resize<ReelInfo>(ref this.reelInfo, 5);
        int a = 0;
        while (a < 5)
        {
            this.reelInfo[a] = new ReelInfo();
			System.Array.Resize<SlotInfo>(ref this.reelInfo[a].slotOrder, 3);
            int b = 0;
            while (b < 3)
            {
                this.reelInfo[a].slotOrder[b] = new SlotInfo();
                this.reelInfo[a].slotOrder[b].ID = this.GenerateNewID();
                b++;
            }
            a++;
        }

        this.scatterObject.SetActive(false);
        this.coinShower.enableEmission = false;
        this.StartCoroutine(this.GenerateNewColumns());
        this.GenerateLineInfo();
        this.lineCount = this.linesInfo.lineInfo.Length;
        this.PopulateFaceIcons(false);
        this.UpdateText();
        this.fadeValue = 1;
        Music.THIS.GetComponent<AudioSource>().Stop();
        Music.THIS.GetComponent<AudioSource>().loop = true;
        Music.THIS.GetComponent<AudioSource>().clip = Music.THIS.music[1];
        Music.THIS.GetComponent<AudioSource>().Play();
        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Drop);
    }

    void Start()
    {
        EnableCamera();
        // SlotCam.SetActive(true);
        if (PlayerPrefs.GetInt("LastWin") > 0)
        {
           
            LastWin.text = " WELCOME BACK!!!";
        }
        else
            LastWin.text = "WELCOME TO SLOT MACHINE!";
      
    }

    void EnableCamera()
    {
        float _check = (float)Screen.width / (float)Screen.height;

        float Check = (float)Mathf.Round(_check * 100) / 100f;
        if (Check == 2.16f || Check == 2.17f)
        {
            CoinPanel.transform.localPosition = new Vector3(25.25f, 12.48f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 12.72f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-24.39f, 12.52f, 0);
            PayTableObj.transform.localPosition = new Vector3(-24.39f, -12.16f, 0);


            SpinObj.transform.localPosition = new Vector3(22.5f, -12.11f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(14.57f, -12.11f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(14.57f, -12.11f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(14.57f, -12.11f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0, -12.14f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.15f, -11.4f, 0);
            LineObj.transform.localPosition = new Vector3(-14.46f, -11.4f, 0);
           

        }
        else if (Check == 2.06f)
        {
            CoinPanel.transform.localPosition = new Vector3(23.4f, 12.4f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 12.69f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-23.11f, 12.67f, 0);
            PayTableObj.transform.localPosition = new Vector3(-23.11f, -12.16f, 0);

            SpinObj.transform.localPosition = new Vector3(23.61f, -12.16f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(14.56f, -12.16f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(14.56f, -12.16f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(14.56f, -12.16f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -12.23f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.14f, -11.5f, 0);
            LineObj.transform.localPosition = new Vector3(-14.45f, -11.5f, 0);

        }
        else if (Check == 2.0f)
        {
            CoinPanel.transform.localPosition = new Vector3(23.48f, 12.9f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 13.13f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-23.74f, 12.99f, 0);
            PayTableObj.transform.localPosition = new Vector3(-23.74f, -12.5f, 0);

            SpinObj.transform.localPosition = new Vector3(23.27f, -12.49f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(14.54f, -12.49f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(14.54f, -12.49f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(14.54f, -12.49f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -12.51f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.14f, -11.75f, 0);
            LineObj.transform.localPosition = new Vector3(-14.46f, -11.75f, 0);

        }
        else if (Check == 1.78f)
        {
            CoinPanel.transform.localPosition = new Vector3(18.78f, 12.1f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 12.4f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-21.14f, 12.26f, 0);
            PayTableObj.transform.localPosition = new Vector3(-21.14f, -12.168f, 0);


            SpinObj.transform.localPosition = new Vector3(21.2f, -12.16f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(13.74f, -12.16f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(13.74f, -12.16f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(13.74f, -12.16f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -12.23f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.06f, -11.5f, 0);
            LineObj.transform.localPosition = new Vector3(-14.4f, -11.5f, 0);
        }
        else if (Check == 1.67f)
        {
            CoinPanel.transform.localPosition = new Vector3(18.09f, 12.56f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 12.8f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-20.54f, 12.71f, 0);
            PayTableObj.transform.localPosition = new Vector3(-20.54f, -12.16f, 0);


            SpinObj.transform.localPosition = new Vector3(20.62f, -12.21f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(14.53f, -12.21f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(14.53f, -12.21f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(14.53f, -12.21f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -12.25f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.12f, -11.5f, 0);
            LineObj.transform.localPosition = new Vector3(-14.4f, -11.5f, 0);
        }
        else if (Check == 1.5f)
        {
            CoinPanel.transform.localPosition = new Vector3(18.24f, 14.31f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 14.53f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-20.99f, 14.35f, 0);
            PayTableObj.transform.localPosition = new Vector3(-20.99f, -12.95f, 0);


            SpinObj.transform.localPosition = new Vector3(20.92f, -12.95f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(14.65f, -12.95f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(14.65f, -12.95f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(14.65f, -12.95f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -13f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.12f, -12.27f, 0);
            LineObj.transform.localPosition = new Vector3(-14.4f, -12.27f, 0);
        }
        else if (Check == 1.43f)
        {
            CoinPanel.transform.localPosition = new Vector3(15.16f, 12.91f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 13.17f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-18.75f, 12.99f, 0);
            PayTableObj.transform.localPosition = new Vector3(-18.75f, -12.46f, 0);


            SpinObj.transform.localPosition = new Vector3(18.2f, -12.46f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(12.43f, -12.46f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(12.43f, -12.46f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(12.43f, -12.46f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -12.53f, 0);
            BetLineObj.transform.localPosition = new Vector3(-7.64f, -11.79f, 0);
            LineObj.transform.localPosition = new Vector3(-13.39f, -11.79f, 0);
        }
        else if (Check == 1.33f)
        {
            CoinPanel.transform.localPosition = new Vector3(17.64f, 15.83f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 16.13f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-21.38f, 16.05f, 0);
            PayTableObj.transform.localPosition = new Vector3(-21.38f, -14.09f, 0);


            SpinObj.transform.localPosition = new Vector3(20.66f, -14.09f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(13.74f, -14.09f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(13.74f, -14.09f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(13.74f, -14.09f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -14.17f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.25f, -13.44f, 0);
            LineObj.transform.localPosition = new Vector3(-14.4f, -13.44f, 0);
        }
        else
        {
            CoinPanel.transform.localPosition = new Vector3(18.78f, 12.1f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 12.4f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-22.76f, 12.26f, 0);
            PayTableObj.transform.localPosition = new Vector3(-22.76f, -12.45f, 0);


            SpinObj.transform.localPosition = new Vector3(23.61f, -12.16f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(14.56f, -12.16f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(14.56f, -12.16f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(14.56f, -12.16f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -12.23f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.14f, -11.5f, 0);
            LineObj.transform.localPosition = new Vector3(-14.45f, -11.5f, 0);
        }
    }

    public virtual void Update()
    {

        //If we left click
        if (Input.GetMouseButtonDown(0))
        {
            this.Click(Input.mousePosition);
        }
        if ((Input.touchCount > 0) && !this.touching)
        {
            if ((Input.GetTouch(0).phase == TouchPhase.Began) && !this.touching)
            {
                this.touching = true;
                this.Click(Input.GetTouch(0).position);
      
            }
        }
        if ((((TouchPhase) Input.touchCount) == TouchPhase.Ended) || (((TouchPhase) Input.touchCount) == TouchPhase.Canceled))
        {
            if (this.touching)
            {
                this.touching = false;
            }
        }
        if (this.displayWinningEffects)
        {
            if ((DataManager.Instance.FluxCoins != DataManager.Instance.Coins)) //&& (this.coinShower.enableEmission == false)
            {
               // Debug.Log("_________Showing effect winnnnnnnnnn!!!!!");

            }
            else if ((DataManager.Instance.FluxCoins == DataManager.Instance.Coins)) //&& (this.coinShower.enableEmission == true)
            {
                Debug.Log("_______________________________________________________________________________________________Showed effect winnnnnnnnnn DONE!!!!!!");
                isAddCoins = true;

            }
            this.effectsTimer = this.effectsTimer + Time.deltaTime;
            foreach (LineInfo line in this.linesInfo.lineInfo)
            {
                if (line.winner)
                {
                    if ((this.effectsTimer < 0.75f) && !line.lineParent.activeSelf)
                    {
                        line.lineParent.SetActive(true);
                    }
                    if (this.effectsTimer > 0.75f)
                    {
                        if (line.lineParent.activeSelf)
                        {
                            line.lineParent.SetActive(false);
                        }
                        if (this.effectsTimer >= 1)
                        {
                            this.effectsTimer = 0f;
                        }
                    }
                }
            }
        }
        if (this.scattersLeft > 0)
        {
            if (this.scatterCountObject.text != this.scattersLeft.ToString())
            {
                this.scatterCountObject.text = this.scattersLeft.ToString();
            }
            this.EngageScatters();
        }
        if (this.isAutoSpin)
        {
            // auto spin
            this.EngageScatters1();

        }
        if (this.spinning)
        {
            int h = 0;
            while (h < 5)
            {
                int i = 0;
                while (i < 3)
                {
                    if (!this.reelInfo[h].slotOrder[i].collided)
                    {
                        if ((this.reelInfo[h].slotOrder[i].sprite.gameObject.GetComponent<Rigidbody2D>().velocity.y >= 0) && (this.reelInfo[h].slotOrder[i].sprite.transform.position.y < 5))
                        {
                            this.reelInfo[h].slotOrder[i].collided = true;
                        }
                    }
                    if (this.reelInfo[h].slotOrder[i].collided)
                    {
                        if (this.reelInfo[h].slotOrder[i].sprite.gameObject.GetComponent<Rigidbody2D>().velocity.y < -0.1f)
                        {
                            this.reelInfo[h].slotOrder[i].collided = false;
                        }
                    }
                    i++;
                }
                h++;
            }
            if ((this.reelInfo[4].slotOrder[2].sprite.gameObject.GetComponent<Rigidbody2D>().velocity.y >= 0) && (this.reelInfo[4].slotOrder[2].sprite.transform.position.y < 5))
            {
                this.StartCoroutine(this.CheckForWinningSymbols());
            }
        }
    }

    public virtual void Click(Vector3 position)
    {
        RaycastHit hit = default(RaycastHit);
        Ray ray = this.GetComponent<Camera>().ScreenPointToRay(position);
        if (Physics.Raycast(ray, out hit, 100))
        {
            foreach (ButtonInfo button in this.buttonInfo)
            {
                if (button.sprite != null&& (button.sprite.GetComponent<SpriteRenderer>().material.color != Color.gray))
                {
                    if (hit.transform == button.sprite.transform)
                    {
                        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.ButtonClick);
                        if (button.functionType == ButtonInfo.FunctionType.Spin)
                        {
                            if (this.scattersLeft == 0)
                            {
                                this.gameObject.SendMessage(button.functionType.ToString(), this.lineCount * this.betAmounts[(int)this.currentBet]);
                            }
                        }
                        else
                        {
                            this.Invoke(button.functionType.ToString(), 0f);
                        }
                    }
                }
            }
        }
    }

    public virtual IEnumerator GenerateNewColumns()
    {
        if (this.currentSymbolBundle != null)
        {
            UnityEngine.Object.Destroy(this.currentSymbolBundle);
        }
        this.currentSymbolBundle = new GameObject();
        this.currentSymbolBundle.name = "Symbols";
        int c = 0;
        while (c < 5)
        {
            int s = 0;
            while (s < 3)
            {
                this.SpawnSymbol(c, s);
                yield return new WaitForSeconds(0.05f);
                s++;
            }
            c++;
        }
        this.generatingSymbols = false;
        this.spinning = true;
    }
    public void ButtonClicked()
    {
        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.ButtonClick);
    }
    public virtual void GenerateNewLine()
    {
        if (!this.lines)
        {
            GameObject lineGrandparent = new GameObject();
            lineGrandparent.name = "LineManager";
            this.lines = lineGrandparent;
        }
        GameObject lineParent = new GameObject();
        lineParent.AddComponent<LineRenderer>();
        lineParent.name = "NewLine";
        int l = 0;
        while (l < 5)
        {
            GameObject lineChild = new GameObject();
            lineChild.AddComponent<LineRenderer>();
            lineChild.name = "Segment " + l.ToString();
            lineChild.transform.parent = lineParent.transform;
            l++;
        }
        lineParent.transform.parent = this.lines.transform;
        lineParent.SetActive(false);
        this.generatingLine = false;
    }

    public virtual void GenerateLineInfo()
    {
        int a = 0;
        while (a < 5)
        {
            int b = 0;
            while (b < 3)
            {
                this.linePositions[(a * 3) + b] = new Vector3(this.elbowPositions[(a * 3) + b].x, this.elbowPositions[(a * 3) + b].y, -0.5f);
                b++;
            }
            a++;
        }
        int i = 0;
        while (i < this.linesInfo.lineInfo.Length)
        {
            this.linesInfo.lineInfo[i].lineParent.SetActive(false);
            this.linesInfo.lineInfo[i].lineParent.GetComponent<Renderer>().material = new Material(this.linesInfo.lineShader);
            this.linesInfo.lineInfo[i].lineParent.GetComponent<Renderer>().sharedMaterial.color = this.linesInfo.lineInfo[i].thisColor;
            System.Array.Resize<int>(ref this.linesInfo.lineInfo[i].lineNumbers, 5);
            this.linesInfo.lineInfo[i].lineNumbers[0] = (int) this.linesInfo.lineInfo[i].LineNumbers.firstReel;
            this.linesInfo.lineInfo[i].lineNumbers[1] = (int) (this.linesInfo.lineInfo[i].LineNumbers.secondReel + 3);
            this.linesInfo.lineInfo[i].lineNumbers[2] = (int) (this.linesInfo.lineInfo[i].LineNumbers.thirdReel + 6);
            this.linesInfo.lineInfo[i].lineNumbers[3] = (int) (this.linesInfo.lineInfo[i].LineNumbers.forthReel + 9);
            this.linesInfo.lineInfo[i].lineNumbers[4] = (int) (this.linesInfo.lineInfo[i].LineNumbers.fifthReel + 12);
            if (this.linesInfo.lineInfo[i].lineBoxPosition.x > 0)
            {
                this.ReverseLineVisuals(i);
            }
            else
            {
                this.LineVisuals(i);
            }
            i++;
        }
    }

    public virtual void LineVisuals(int ID)
    {
        ((LineRenderer) this.linesInfo.lineInfo[ID].lineParent.GetComponent(typeof(LineRenderer))).SetPosition(0, this.linesInfo.lineInfo[ID].lineBoxPosition + new Vector3(0.5f, 0, -0.5f));
        ((LineRenderer) this.linesInfo.lineInfo[ID].lineParent.GetComponent(typeof(LineRenderer))).SetPosition(1, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[0]]);
        ((LineRenderer) this.linesInfo.lineInfo[ID].lineParent.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
        Transform parentObject = null;
        parentObject = this.linesInfo.lineInfo[ID].lineParent.transform;
        int i = 0;
        while (i < 5)
        {
            Transform child = null;
            child = this.linesInfo.lineInfo[ID].lineParent.transform.GetChild(i);
            child.GetComponent<Renderer>().material = parentObject.gameObject.GetComponent<Renderer>().sharedMaterial;
            ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetPosition(0, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[i]]);
            ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
            if (i < 4)
            {
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetPosition(1, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[i + 1]]);
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
            }
            else
            {
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetPosition(1, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[i]] + new Vector3(5, 0, -0.5f));
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
            }
            parentObject = child;
            i++;
        }
    }

    public virtual void ReverseLineVisuals(int ID)
    {
        ((LineRenderer) this.linesInfo.lineInfo[ID].lineParent.GetComponent(typeof(LineRenderer))).SetPosition(0, this.linesInfo.lineInfo[ID].lineBoxPosition - new Vector3(0.5f, 0, -0.5f));
        ((LineRenderer) this.linesInfo.lineInfo[ID].lineParent.GetComponent(typeof(LineRenderer))).SetPosition(1, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[4]]);
        ((LineRenderer) this.linesInfo.lineInfo[ID].lineParent.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
        Transform parentObject = null;
        parentObject = this.linesInfo.lineInfo[ID].lineParent.transform;
        int i = 4;
        while (i > -1)
        {
            Transform child = null;
            child = this.linesInfo.lineInfo[ID].lineParent.transform.GetChild(i);
            child.GetComponent<Renderer>().sharedMaterial = parentObject.GetComponent<Renderer>().sharedMaterial;
            ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetPosition(0, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[i]]);
            ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
            if (i > 0)
            {
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetPosition(1, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[i - 1]]);
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
            }
            else
            {
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetPosition(1, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[i]] - new Vector3(5, 0, -0.5f));
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
            }
            parentObject = child;
            i--;
        }
    }

    public virtual void EngageScatters()
    {
        if (!this.scatterObject.activeSelf)
        {
            this.scatterObject.SetActive(true);
        }
        if (!this.spinning)
        {
            this.scatterTimer = this.scatterTimer - Time.deltaTime;
        }
        if (this.spinning && (this.scatterTimer != 3))
        {
            this.scatterTimer = 3;
        }
        if (this.scatterTimer < 0)
        {
            this.StartCoroutine(this.Spin(0f));
            this.scatterTimer = 3;
            this.scattersLeft = this.scattersLeft - 1;
            if (this.scattersLeft == 0)
            {
                this.scatterObject.SetActive(false);
            }
        }
    }
    public virtual void EngageScatters1()
    {
  
        if (!this.spinning)
        {
            this.autoSpinTimer = this.autoSpinTimer - Time.deltaTime;
        }
        if (this.spinning && (this.autoSpinTimer != 1))
        {
            this.autoSpinTimer = 1;
        }
        if (this.autoSpinTimer < 0)
        {
            this.StartCoroutine(this.AutoSpinMehod(this.lineCount * this.betAmounts[(int)this.currentBet]));
        }
    }
    public virtual IEnumerator AutoSpinMehod(float Deduction)
    {
        int t = 0;
        if (((!this.spinning && !this.dropping) && !this.generatingSymbols))
        {
            if (Deduction <= DataManager.Instance.Coins)
            {
                PlayerPrefs.SetInt("SpinTimes", PlayerPrefs.GetInt("SpinTimes") + 1);
                FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Spin2);
                isAddCoins = false;
                //--Last win--------------
                if (PlayerPrefs.GetInt("LastWin") > 0)
                    LastWin.text = "YOUR LAST WIN: " + PlayerPrefs.GetInt("LastWin").ToString(("n0"));
                //------------------------

                DataManager.Instance.RemoveCoins((int)Deduction);
                DataManager.Instance.ResetFluxCoins();
                CoinsTxt.text = DataManager.Instance.FluxCoins.ToString("n0");
                PlayerPrefs.Save();
                this.dropping = true;
                this.totalPayout = 0f;
                this.DisableAllVisuals();

                this.UpdateText();
                this.DarkenButtons();
                this.generatingSymbols = true;
                t = 0;
                while (t < 5)
                {
                    this.trapDoors[t].gameObject.SetActive(false);
                    yield return new WaitForSeconds(0.1f);
                    t++;
                }
                yield return new WaitForSeconds(0.5f);
                this.PopulateFaceIcons(true);
                this.CalculatePayout();
                this.StartCoroutine(this.GenerateNewColumns());
                t = 0;
                while (t < 5)
                {
                    this.trapDoors[t].gameObject.SetActive(true);
                    t++;
                }
            }
        }
    }
    public virtual void AutoSpin()
    {
        if (!this.isAutoSpin && !this.spinning)
        {
            this.isAutoSpin = true;
            AutoSpinEffect.SetActive(true);
            Auto.SetActive(false);
            Stop.SetActive(true);
        }
        else if (this.isAutoSpin && !this.spinning)
        {
            this.isAutoSpin = false;
            AutoSpinEffect.SetActive(false);
            Auto.SetActive(true);
            Stop.SetActive(false);
        }

    }
    public virtual void StopAuto()
    {
        if (!this.isAutoSpin && !this.spinning)
        {
            this.isAutoSpin = true;
            AutoSpinEffect.SetActive(true);
            Auto.SetActive(false);
            Stop.SetActive(true);
        }
        else if (this.isAutoSpin && !this.spinning)
        {
            this.isAutoSpin = false;
            AutoSpinEffect.SetActive(false);
            Auto.SetActive(true);
            Stop.SetActive(false);
        }

    }


    public virtual void IncreaseLines()
    {
        if ((!this.spinning && (this.scattersLeft == 0)) && !this.dropping)
        {
            if (this.lineCount < this.linesInfo.lineInfo.Length)
            {
                this.lineCount = this.lineCount + 1;
            }
            else
            {
                this.lineCount = 1;
            }
            this.UpdateText();
            this.SetVisuals();
        }
    }

    public virtual void DecreaseLines()
    {
        if ((!this.spinning && (this.scattersLeft == 0)) && !this.dropping)
        {
            if (this.lineCount > 1)
            {
                this.lineCount = this.lineCount - 1;
            }
            else
            {
                this.lineCount = this.linesInfo.lineInfo.Length;
            }
            this.UpdateText();
            this.SetVisuals();
        }
    }

    public virtual void DisableAllVisuals()
    {
        this.displayWinningEffects = false;
        int a = 1;
        while (a < (this.linesInfo.lineInfo.Length + 1))
        {
            this.linesInfo.lineInfo[a - 1].lineParent.SetActive(false);
            a++;
        }
        this.coinShower.enableEmission = false;
    }

    public virtual void SetVisuals()
    {
        this.displayWinningEffects = false;
        int a = 1;
        while (a < (this.linesInfo.lineInfo.Length + 1))
        {
            if (a <= this.lineCount)
            {
                this.linesInfo.lineInfo[a - 1].lineParent.SetActive(true);
            }
            if (a > this.lineCount)
            {
                this.linesInfo.lineInfo[a - 1].lineParent.SetActive(false);
            }
            a++;
        }
    }

    public virtual void IncreaseBet()
    {
        if ((!this.spinning && (this.scattersLeft == 0)) && !this.dropping)
        {
            if (this.currentBet == this.maxBet)
            {
                this.currentBet = 0;
            }
            else
            {
                this.currentBet = this.currentBet + 1;
            }
            this.UpdateText();
        }
    }

    public virtual void DecreaseBet()
    {
        if ((!this.spinning && (this.scattersLeft == 0)) && !this.dropping)
        {
            if (this.currentBet == 0)
            {
                this.currentBet = this.maxBet;
            }
            else
            {
                this.currentBet = this.currentBet - 1;
            }
            this.UpdateText();
        }
    }

    public virtual IEnumerator Spin(float Deduction)
    {
        int t = 0;
        if (((!this.spinning && !this.dropping) && !this.generatingSymbols))
        {
			if (Deduction <= DataManager.Instance.Coins)
            {
                FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Spin2);
                PlayerPrefs.SetInt("SpinTimes", PlayerPrefs.GetInt("SpinTimes") + 1);
                isAddCoins = false;
                //--Last win--------------
                if (PlayerPrefs.GetInt("LastWin")>0)
                    LastWin.text = "YOUR LAST WIN: " + PlayerPrefs.GetInt("LastWin").ToString(("n0"));
                //------------------------

                DataManager.Instance.RemoveCoins((int)Deduction);
                DataManager.Instance.ResetFluxCoins();
                CoinsTxt.text = DataManager.Instance.FluxCoins.ToString("n0");
                PlayerPrefs.Save();
                this.dropping = true;
                this.totalPayout = 0f;
                this.DisableAllVisuals();

                this.UpdateText();
                this.DarkenButtons();
                this.generatingSymbols = true;
                t = 0;
                while (t < 5)
                {
                    this.trapDoors[t].gameObject.SetActive(false);
                    yield return new WaitForSeconds(0.1f);
                    t++;
                }
                yield return new WaitForSeconds(0.5f);
                this.PopulateFaceIcons(true);
                this.CalculatePayout();
                this.StartCoroutine(this.GenerateNewColumns());
                t = 0;
                while (t < 5)
                {
                    this.trapDoors[t].gameObject.SetActive(true);
                    t++;
                }
            }
        }
    }

    public virtual void PopulateFaceIcons(bool Repopulate)
    {
        int y = 0;
        while (y < 5)
        {
            int z = 0;
            while (z < 3)
            {
                if (Repopulate)
                {
                    this.reelInfo[y].slotOrder[z].ID = this.GenerateNewID();
                }
                int topToBottom = 2 - z;
                this.faceIcons[(y * 3) + topToBottom] = this.reelInfo[y].slotOrder[z].ID;
                z++;
            }
            y++;
        }
    }

    public virtual int GenerateNewID()
    {
       
        int randomIcon = 0;
        while (true)
        {
            
            randomIcon = Random.Range(0, this.iconInfo.Length - 1);
			float dividend =((float) this.iconInfo[randomIcon].frequency) + 1;
            float randomValue = Random.value;
            if ((1 / dividend) > randomValue)
            {
                break;
            }
        }
        return randomIcon;
    }

    public virtual void CalculatePayout()
    {
        int scatterCount = 0;
        foreach (LineInfo line in this.linesInfo.lineInfo)
        {
            line.lineParent.SetActive(false);
            line.winningValue = 0;
            line.winner = false;
        }
        int s = 0;
        while (s < this.faceIcons.Length)
        {
			if (this.iconInfo[this.faceIcons[s]].iconType ==  IconInfo.IconType.Scatter)
            {
                scatterCount = scatterCount + 1;
            }
            if (s == (this.faceIcons.Length - 1))
            {
                if (scatterCount >= 3)
                {
                    int scatterMultiplier = scatterCount - 2;
                    this.tempScatter = scatterCount * scatterMultiplier;
                }
            }
            s++;
        }
        int a = 0;
        while (a < this.lineCount)
        {
            PayoutInfo payoutIcon = new PayoutInfo();
            if (this.payoutOrder == (PayoutOrder) 0)
            {
				if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[0]]].iconType !=  IconInfo.IconType.Wild)
                {
                    payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[0]];
                }
				if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[0]]].iconType ==  IconInfo.IconType.Wild)
                {
                    int wB = 1;
                    while (wB < 5)
                    {
						if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wB]]].iconType ==  IconInfo.IconType.Normal)
                        {
                            payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wB]];
                            wB = 4;
                            break;
                        }
                        if (wB < 4)
                        {
							if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wB]]].iconType ==  IconInfo.IconType.Wild)
                            {
                                goto Label_for_22;
                            }
                        }
                        if (wB == 4)
                        {
							if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wB]]].iconType ==  IconInfo.IconType.Wild)
                            {
                                payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[0]];
                                break;
                            }
                        }
						if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wB]]].iconType ==  IconInfo.IconType.Scatter)
                        {
                            payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[0]];
                            wB = 4;
                            break;
                        }
                        Label_for_22:
                        wB++;
                    }
                }
                int b = 0;
                while (b < 5)
                {
                    payoutIcon.amount = payoutIcon.amount + 1;
                    if (this.payoutOrder == (PayoutOrder) 0)
                    {
                        if (b < 4)
                        {
							if ((payoutIcon.ID != this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[b + 1]]) && (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[b + 1]]].iconType !=  IconInfo.IconType.Wild))
                            {
                                int Aamount = b + 1;
                                int AlIne = a + 1;
                                b = 4;
                                break;
                            }
                        }
                    }
                    b++;
                }
            }
            if (this.payoutOrder == (PayoutOrder) 1)
            {
				if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[4]]].iconType !=  IconInfo.IconType.Wild)
                {
                    payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[4]];
                }
                if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[4]]].iconType == IconInfo.IconType.Wild)
                {
                    int wC = 3;
                    while (wC > -1)
                    {
						if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wC]]].iconType ==  IconInfo.IconType.Normal)
                        {
                            payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wC]];
                            wC = 0;
                            break;
                        }
                        if (wC > 0)
                        {
							if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wC]]].iconType ==  IconInfo.IconType.Wild)
                            {
                                goto Label_for_24;
                            }
                        }
                        if (wC == 0)
                        {
							if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wC]]].iconType ==  IconInfo.IconType.Wild)
                            {
                                payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[4]];
                                break;
                            }
                        }
						if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wC]]].iconType ==  IconInfo.IconType.Scatter)
                        {
                            payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[4]];
                            wC = 0;
                            break;
                        }
                        Label_for_24:
                        wC--;
                    }
                }
                int c = 4;
                while (c > -1)
                {
                    payoutIcon.amount = payoutIcon.amount + 1;
                    if (c > 0)
                    {
						if ((payoutIcon.ID != this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[c - 1]]) && (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[c - 1]]].iconType !=  IconInfo.IconType.Wild))
                        {
                            int Damount = 5 - c;
                            int DlIne = a + 1;
                            c = 0;
                            break;
                        }
                    }
                    c--;
                }
            }
            if (this.iconInfo[payoutIcon.ID].xTwo > 0)
            {
                if (payoutIcon.amount == 2)
                {
					if ((this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Normal) || (this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Wild))
                    {
                        this.linesInfo.lineInfo[a].winningValue = this.iconInfo[payoutIcon.ID].xTwo * this.betAmounts[this.currentBet];
                        this.linesInfo.lineInfo[a].winner = true;
                        this.linesInfo.lineInfo[a].winningIconIDs = new int[2];
                        if (this.payoutOrder == (PayoutOrder) 0)
                        {
                            this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.firstReel;
                            this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.secondReel;
                        }
                        if (this.payoutOrder == (PayoutOrder) 1)
                        {
                            this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.fifthReel;
                            this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.forthReel;
                        }
                    }
                }
            }
            if (payoutIcon.amount == 3)
            {
				if ((this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Normal) || (this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Wild))
                {
                    this.linesInfo.lineInfo[a].winningValue = this.iconInfo[payoutIcon.ID].xThree * this.betAmounts[this.currentBet];
                    this.linesInfo.lineInfo[a].winner = true;
                    this.linesInfo.lineInfo[a].winningIconIDs = new int[3];
                    if (this.payoutOrder == (PayoutOrder) 0)
                    {
                        this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.firstReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.secondReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[2] = (int) this.linesInfo.lineInfo[a].LineNumbers.thirdReel;
                    }
                    if (this.payoutOrder == (PayoutOrder) 1)
                    {
                        this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.fifthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.forthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[2] = (int) this.linesInfo.lineInfo[a].LineNumbers.thirdReel;
                    }
                }
            }
            if (payoutIcon.amount == 4)
            {
				if ((this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Normal) || (this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Wild))
                {
                    this.linesInfo.lineInfo[a].winningValue = this.iconInfo[payoutIcon.ID].xFour * this.betAmounts[this.currentBet];
                    this.linesInfo.lineInfo[a].winner = true;
                    this.linesInfo.lineInfo[a].winningIconIDs = new int[4];
                    if (this.payoutOrder == (PayoutOrder) 0)
                    {
                        this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.firstReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.secondReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[2] = (int) this.linesInfo.lineInfo[a].LineNumbers.thirdReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[3] = (int) this.linesInfo.lineInfo[a].LineNumbers.forthReel;
                    }
                    if (this.payoutOrder == (PayoutOrder) 1)
                    {
                        this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.fifthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.forthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[2] = (int) this.linesInfo.lineInfo[a].LineNumbers.thirdReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[3] = (int) this.linesInfo.lineInfo[a].LineNumbers.secondReel;
                    }
                }
            }
            if (payoutIcon.amount == 5)
            {
				if ((this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Normal) || (this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Wild))
                {
                    this.linesInfo.lineInfo[a].winningValue = this.iconInfo[payoutIcon.ID].xFive * this.betAmounts[this.currentBet];
                    this.linesInfo.lineInfo[a].winner = true;
                    this.linesInfo.lineInfo[a].winningIconIDs = new int[5];
                    if (this.payoutOrder == (PayoutOrder) 0)
                    {
                        this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.firstReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.secondReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[2] = (int) this.linesInfo.lineInfo[a].LineNumbers.thirdReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[3] = (int) this.linesInfo.lineInfo[a].LineNumbers.forthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[4] = (int) this.linesInfo.lineInfo[a].LineNumbers.fifthReel;
                    }
                    if (this.payoutOrder == (PayoutOrder) 1)
                    {
                        this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.fifthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.forthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[2] = (int) this.linesInfo.lineInfo[a].LineNumbers.thirdReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[3] = (int) this.linesInfo.lineInfo[a].LineNumbers.secondReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[4] = (int) this.linesInfo.lineInfo[a].LineNumbers.firstReel;
                    }
                }
            }
            a++;
        }
    }

    public virtual IEnumerator ExplodeSymbol(int r, int n)
    {
      
        yield return new WaitForSeconds(1);
        UnityEngine.Object.Instantiate(this.explosionParticle, this.reelInfo[r].slotOrder[2 - n].sprite.transform.position + new Vector3(this.reelInfo[r].slotOrder[2 - n].size.x / 2, this.reelInfo[r].slotOrder[2 - n].size.y / 2, 0), Quaternion.Euler(0, 180, 0));
        UnityEngine.Object.Destroy(this.reelInfo[r].slotOrder[2 - n].sprite);
    }

    public virtual IEnumerator CheckForWinningSymbols()
    {
        int l = 0;
        int n = 0;
        float payout = 0f;
        string[] winningSlots = new string[15];
        foreach (LineInfo line in this.linesInfo.lineInfo)
        {
            if (line.winningValue > 0f)
            {
                payout = payout + line.winningValue;
               
            }
            if (line.winner)
            {
                if (!this.displayWinningEffects)
                {
                    this.displayWinningEffects = true;
                }
                int i = 0;
                while (i < line.winningIconIDs.Length)
                {
                    int inSet = i * 3;
                    if (this.payoutOrder == (PayoutOrder) 0)
                    {
                        this.reelInfo[i].slotOrder[(this.reelInfo[i].slotOrder.Length - 1) - line.winningIconIDs[i]].canAnimate = true;
                        winningSlots[(i * 3) + line.winningIconIDs[i]] = "Destroy";
                    }
                    if (this.payoutOrder == (PayoutOrder) 1)
                    {
                        this.reelInfo[4 - i].slotOrder[(this.reelInfo[4 - i].slotOrder.Length - 1) - line.winningIconIDs[i]].canAnimate = true;
                        winningSlots[(12 - inSet) + line.winningIconIDs[i]] = "Destroy";
                    }
                    i++;
                }
                line.winningIconIDs = new int[0];
            }
           
        }
        bool destroySymbols = false;

        if (payout > 0f)
        {

            DataManager.Instance.AddCoins((int)payout);
            DataManager.Instance.ResetFluxCoins();
            CoinsTxt.text = DataManager.Instance.FluxCoins.ToString("n0");
            totalPayout += payout;
            FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Win2);
           
            if (isAddCoins)
            {
                PlayerPrefs.SetInt("LastWin", PlayerPrefs.GetInt("LastWin") + (int)totalPayout);
                isAddCoins = false;
            }
            else
            {
                PlayerPrefs.SetInt("LastWin", (int)totalPayout);
            }
            PlayerPrefs.Save();
            destroySymbols = true;
        }
        int a = 0;
        while (a < this.reelInfo.Length)
        {
            this.StartCoroutine(this.CheckForAnimatedIcons(a, this.reelInfo[a].slotOrder.Length - 1));
            this.StartCoroutine(this.CheckForAnimatedIcons(a, this.reelInfo[a].slotOrder.Length - 2));
            this.StartCoroutine(this.CheckForAnimatedIcons(a, this.reelInfo[a].slotOrder.Length - 3));
            a++;
        }
      
        this.UpdateText();
        l = 0;
        while (l < 5)
        {
            n = 0;
            while (n < 3)
            {
                if (winningSlots[(l * 3) + n] == "Destroy")
                {
                    this.StartCoroutine(this.ExplodeSymbol(l, n));
                }
                n++;
            }
            l++;
        }
        if (!destroySymbols)
        {
          
            this.EndSpin();
          
        }
        this.spinning = false;
        if (destroySymbols)
        {
           
            yield return new WaitForSeconds(1.1f);
            // Explosion
            FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Explosion);
            this.DisableAllVisuals();
        }
        l = 0;
        while (l < 5)
        {
            n = 0;
            while (n < 3)
            {
                this.reelInfo[l].slotOrder[n].canAnimate = false;
                this.reelInfo[l].slotOrder[n].animating = false;
                Vector3 newSymbols = new Vector3(this.GenerateNewID(), this.GenerateNewID(), this.GenerateNewID());
                if (n == 0)
                {
                    if (this.reelInfo[l].slotOrder[0].sprite == null)
                    {
                        if (this.reelInfo[l].slotOrder[1].sprite == null)
                        {
                            if (this.reelInfo[l].slotOrder[2].sprite == null)
                            {
                                this.reelInfo[l].slotOrder[0].ID = (int) newSymbols.x;
                                this.reelInfo[l].slotOrder[1].ID = (int) newSymbols.y;
                                this.reelInfo[l].slotOrder[2].ID = (int) newSymbols.z;
                                this.SpawnSymbol(l, 0);
                                this.SpawnSymbol(l, 1);
                                this.SpawnSymbol(l, 2);
                                break;
                            }
                            if (this.reelInfo[l].slotOrder[2].sprite != null)
                            {
                                this.reelInfo[l].slotOrder[0].sprite = this.reelInfo[l].slotOrder[2].sprite;
                                this.reelInfo[l].slotOrder[0].ID = this.reelInfo[l].slotOrder[2].ID;
                                this.reelInfo[l].slotOrder[2].sprite = null;
                                this.reelInfo[l].slotOrder[1].ID = (int) newSymbols.y;
                                this.reelInfo[l].slotOrder[2].ID = (int) newSymbols.z;
                                this.SpawnSymbol(l, 1);
                                this.SpawnSymbol(l, 2);
                                goto Label_for_31;
                            }
                        }
                        if (this.reelInfo[l].slotOrder[1].sprite != null)
                        {
                            this.reelInfo[l].slotOrder[0].sprite = this.reelInfo[l].slotOrder[1].sprite;
                            this.reelInfo[l].slotOrder[0].ID = this.reelInfo[l].slotOrder[1].ID;
                            this.reelInfo[l].slotOrder[1].sprite = null;
                            goto Label_for_31;
                        }
                    }
                }
                if (n == 1)
                {
                    if (this.reelInfo[l].slotOrder[1].sprite == null)
                    {
                        if (this.reelInfo[l].slotOrder[2].sprite == null)
                        {
                            this.reelInfo[l].slotOrder[1].ID = (int) newSymbols.y;
                            this.reelInfo[l].slotOrder[2].ID = (int) newSymbols.z;
                            this.SpawnSymbol(l, 1);
                            this.SpawnSymbol(l, 2);
                            goto Label_for_31;
                        }
                        if (this.reelInfo[l].slotOrder[2].sprite != null)
                        {
                            this.reelInfo[l].slotOrder[1].sprite = this.reelInfo[l].slotOrder[2].sprite;
                            this.reelInfo[l].slotOrder[1].ID = this.reelInfo[l].slotOrder[2].ID;
                            this.reelInfo[l].slotOrder[2].sprite = null;
                            this.reelInfo[l].slotOrder[2].ID = (int) newSymbols.z;
                            this.SpawnSymbol(l, 2);
                            goto Label_for_31;
                        }
                    }
                }
                if (n == 2)
                {
                    if (this.reelInfo[l].slotOrder[2].sprite == null)
                    {
                        this.reelInfo[l].slotOrder[2].ID = (int) newSymbols.z;
                        this.SpawnSymbol(l, 2);
                    }
                    if ((l == 4) && destroySymbols)
                    {
                        yield return new WaitForSeconds(1);
                        this.totalPayout = 0f;
                        this.PopulateFaceIcons(false);
                        this.CalculatePayout();
                        this.spinning = true;
                        destroySymbols = false;
                    }
                }
                yield return new WaitForSeconds(0);
                Label_for_31:
                n++;
            }
            l++;
        }
    }

    public virtual void SpawnSymbol(int r, int s)
    {
        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Drop);
        GameObject newSprite = new GameObject();
        newSprite.AddComponent<SpriteRenderer>();
        this.reelInfo[r].slotOrder[s].sprite = newSprite;
        this.reelInfo[r].slotOrder[s].sprite.name = this.iconInfo[this.reelInfo[r].slotOrder[s].ID].Name;
        this.reelInfo[r].slotOrder[s].sprite.transform.localScale = new Vector3(this.iconSize, this.iconSize, 1);
        newSprite.AddComponent<Rigidbody2D>();
        newSprite.GetComponent<Rigidbody2D>().gravityScale = 15;
        newSprite.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        this.reelInfo[r].slotOrder[s].collided = false;
        if (this.iconInfo.Length > 0)
        {
            this.reelInfo[r].slotOrder[s].sprite.GetComponent<SpriteRenderer>().sprite = this.iconInfo[this.reelInfo[r].slotOrder[s].ID].sprite;
            this.reelInfo[r].slotOrder[s].size = new Vector2(this.reelInfo[r].slotOrder[s].sprite.GetComponent<SpriteRenderer>().bounds.extents.x * 2, this.reelInfo[r].slotOrder[s].sprite.GetComponent<SpriteRenderer>().bounds.extents.y * 2);
            float lowerPos = (this.reelInfo[r].slotOrder[s].sprite.GetComponent<SpriteRenderer>().bounds.extents.y / 2) - (this.trapDoors[0].transform.position.y * 1.05f);
            float split = this.reelInfo[r].slotOrder[s].size.x * 2.5f;
            this.reelInfo[r].slotOrder[s].sprite.transform.position = new Vector3((((r * 1.05f) * this.reelInfo[r].slotOrder[s].size.x) - split) - 0.65f, lowerPos + ((s * 1.25f) * this.reelInfo[r].slotOrder[s].size.y), 0);
            this.reelInfo[r].slotOrder[s].sprite.AddComponent<BoxCollider2D>();
        }
        this.reelInfo[r].slotOrder[s].sprite.transform.parent = this.currentSymbolBundle.transform;
    }

    public virtual void EndSpin()
    {
        if (this.tempScatter > 0)
        {
            this.scatterTimer = 2;
            this.scattersLeft = this.scattersLeft + this.tempScatter;
            this.tempScatter = 0;
            FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Scatter2);
            PlayerPrefs.SetInt("ScatterGames", PlayerPrefs.GetInt("ScatterGames") + 1);
        }
        if (this.scattersLeft == 0)
        {
            this.LightenButtons();
        }
        this.dropping = false;
    }

    public virtual IEnumerator CheckForAnimatedIcons(int r, int s)
    {
        SlotInfo info = this.reelInfo[r].slotOrder[s];
        if (info.canAnimate)
        {
            if (this.iconInfo[info.ID].spriteAnimation.Length > 0)
            {
                int i = 0;
                while (i < this.iconInfo[info.ID].spriteAnimation.Length)
                {
                    if (this.iconInfo[info.ID].spriteAnimation[i] != null)
                    {
                        if (info.sprite)
                        {
                            info.sprite.GetComponent<SpriteRenderer>().sprite = this.iconInfo[info.ID].spriteAnimation[i];
                        }
                    }
                    if (!this.spinning)
                    {
                        yield return new WaitForSeconds(this.iconInfo[info.ID].animatedFramesPerSecond);
                        if (!this.spinning)
                        {
                            if (i == (this.iconInfo[info.ID].spriteAnimation.Length - 1))
                            {
                                info.sprite.GetComponent<SpriteRenderer>().sprite = this.iconInfo[info.ID].spriteAnimation[0];
                                info.canAnimate = false;
                            }
                        }
                    }
                    i++;
                }
            }
        }
    }

    public virtual void UpdateText()
    {
        foreach (DisplayInfo info in this.displayInfo)
        {
            this.gameObject.SendMessage("Update" + info.functionType.ToString(), info.textObject);
        }
    }

    public virtual void UpdateLineCount(TextMesh text)
    {
        text.text = this.lineCount.ToString();
    }

    public virtual void UpdateBetAmount(TextMesh text)
    {
        text.text = this.betAmounts[(int)this.currentBet].ToString();
    }
    public virtual void CloseButton()
    {
        Debug.Log("Close PayTable Clicked");
        this.LightenButtons();
        if (PayTableAnimator.gameObject.activeSelf)
        {
            PayTableAnimator.SetTrigger("PayOff");
        }
    }
    public virtual void Lobby()
    {
        //ads
        //AdmobBannerController.Instance.ShowInterstitial();
        // AllServicesControls.Instance.ShowInterstitials();
        SlotCam.SetActive(false);
        if (LoadingAnimator.gameObject.activeSelf)
        {
            LoadingAnimator.SetTrigger("LoadingOut");
        }
        Debug.Log("Clicked Lobby");

    }

    public virtual void PayTable()
    {
        Debug.Log(" PayTable Clicked");
        this.DarkenButtons();
        this.showPayTable = true;
        payTableImage.SetActive(true);
        if (PayTableAnimator.gameObject.activeSelf)
        {
            PayTableAnimator.SetTrigger("PayOn");
        }
    }



    public virtual void UpdateTotalWin(TextMesh text)
    {
        if (this.totalPayout == 0)
        {
            text.text = "";
        }
        else
        {
            text.text = this.totalPayout.ToString();
        }
    }

    public virtual void UpdateTotalBet(TextMesh text)
    {
        float totalBet = this.lineCount * this.betAmounts[(int)this.currentBet];
        text.text = totalBet.ToString();
    }

    public virtual void DarkenButtons()
    {
        foreach (ButtonInfo button in this.buttonInfo)
        {
            
                if (button.sprite != null && button.functionType != ButtonInfo.FunctionType.CloseButton)
                {
                    if (button.sprite.GetComponent<SpriteRenderer>().material.color != Color.gray)
                    {
                        button.sprite.GetComponent<SpriteRenderer>().material.color = Color.gray;
                    }
                }
            
        }
    }

    public virtual void LightenButtons()
    {
        foreach (ButtonInfo button in this.buttonInfo)
        {
            if (button.sprite != null)
            {
                if (button.sprite.GetComponent<SpriteRenderer>().material.color != Color.white)
                {
                    button.sprite.GetComponent<SpriteRenderer>().material.color = Color.white;
                }
            }
        }
    }

    public virtual void MaxBet()
    {
        if ((!this.dropping && !this.spinning) && (this.scattersLeft == 0))
        {
            int maxedBet = this.GetLargestBet();
            if (maxedBet == 250000)
            {
                return;
            }
            this.lineCount = this.linesInfo.lineInfo.Length;
            this.currentBet = maxedBet;
            this.StartCoroutine(this.Spin(this.lineCount * this.betAmounts[(int)this.currentBet]));
        }
    }
    public virtual int GetLargestBet()
    {
        int i = this.betAmounts.Length - 1;
        while (i >= 0)
        {
            if (DataManager.Instance.Coins >= (this.lineCount * this.betAmounts[i]))
            {
                return i;
            }
            i--;
        }
        return 250000;
    }

    public virtual void OnGUI()
    {
        int l = 0;
        while (l < this.linesInfo.lineInfo.Length)
        {
            GUI.skin.label.alignment = TextAnchor.MiddleCenter;
            GUI.skin.label.fontSize = (int) this.linesInfo.lineNumberSize;
            Vector3 screenPos = this.GetComponent<Camera>().WorldToScreenPoint(this.linesInfo.lineInfo[l].lineBoxPosition);
            Vector2 guiPos = new Vector2(screenPos.x, Screen.height - screenPos.y);
            GUI.color = this.linesInfo.lineInfo[l].thisColor;
            GUI.DrawTexture(new Rect(guiPos.x - (this.linesInfo.lineBoxSize.x / 2), guiPos.y - (this.linesInfo.lineBoxSize.y / 2), this.linesInfo.lineBoxSize.x, this.linesInfo.lineBoxSize.y), this.linesInfo.lineBlock);
            int thisLineNumber = l + 1;
            GUI.color = Color.black;
            GUI.Label(new Rect(guiPos.x - (this.linesInfo.lineBoxSize.x / 2), guiPos.y - (this.linesInfo.lineBoxSize.y / 2), this.linesInfo.lineBoxSize.x, this.linesInfo.lineBoxSize.y), thisLineNumber.ToString());
            l++;
        }
    }

    public DropSlots()
    {
        this.betAmounts = new float[] {1f, 2f, 3f, 5f, 7f, 10f, 20f, 30f, 40f, 60f, 80f, 100f, 150f, 300f, 500f, 1000f, 5000f, 10000f, 15000f, 20000f, 30000f, 50000f, 100000f, 150000f, 200000f, 250000f };
        this.currentBet = 3;
        this.linePositions = new Vector3[15];
    }

}