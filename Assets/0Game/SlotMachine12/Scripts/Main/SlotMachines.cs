using UnityEngine;
using System.Collections;

public enum PayoutOrder
{
    fromLeft = 0,
    fromRight = 1
}

[System.Serializable]
[UnityEngine.ExecuteInEditMode]
public partial class SlotMachines : MonoBehaviour
{
    public GameObject SpinObj;
    public GameObject AutoSpinObj;
    public GameObject StopAutoObj;
    public GameObject AutoSpinEffectObj;
    public GameObject TotalBetObj;
    public GameObject BetLineObj;
    public GameObject LineObj;
    public GameObject LobbyObj;
    public GameObject PayTableObj;
    public GameObject CoinPanel;
    public GameObject Header;
    public Animator PayTableAnimator;
    public GameObject SlotCam;
    public Animator LoadingAnimator;
    public TextMesh LastWin;
    public TextMesh CoinsTxt;
	public GameObject AutoSpinEffect;
	public GameObject Auto;
	public GameObject Stop;
    public ParticleSystem coinShower;
    public GameObject scatterObject;
    public TextMesh scatterCountObject;
	public GameObject payTableImage;
    public PayoutOrder payoutOrder;
    [UnityEngine.Range(3, 100)]
    public int iconsPerReel;
    [UnityEngine.Range(3, 20)]
    public int iconsPerReelDifference;
    [UnityEngine.Range(0, 5)]
    public float iconSize;
    [UnityEngine.Range(50, 200)]
    public float spinSpeed;
    [UnityEngine.Range(0, 5)]
    public int reboundAmount;
    [UnityEngine.Range(0, 50)]
    public int reboundSpeed;
    [UnityEngine.Range(0, 20)]
    public int scatterSize;
    [UnityEngine.Range(3, 25)]
    public int maxBet;
    public float[] betAmounts;
    public IconInfo[] iconInfo;
    public LinesInfo linesInfo;
    public ButtonInfo[] buttonInfo;
    public DisplayInfo[] displayInfo;
    [UnityEngine.HideInInspector]
    public ReelInfo[] reelInfo;
    [UnityEngine.HideInInspector]
    public GameObject lines;
    [UnityEngine.HideInInspector]
    public int tempBonusState;
    [UnityEngine.HideInInspector]
    public int tempScatter;
    [UnityEngine.HideInInspector]
    public bool spinning;
    private Vector3[] linePositions;
    private bool displayWinningEffects;
    private bool scatterSpinning;
    private bool generatingLine;
    private bool showPayTable;
    private bool inBonusGame;
    private bool iconsSet;
    private int prevIconCount;
    private int scattersLeft;
    private float bonusWinnings;
    private int lineCount;
    private int[] prevFaceIcons;
    private int[] faceIcons;
    private int[] faceSprites;
    private float curSpinSpeed;
    private float scatterTimer;
    private float autoTimer;
    private float effectsTimer;
    private float totalPayout;
    private float targetPos;
    private float currentBet;
    private float fadeValue;
	private int picks;
	public static int picks1;
    private bool touching;
	public static float bonuscoins;
	private bool isAutoSpin=false;
    private bool isAddCoins = false;
    public virtual void Awake()
    {
        SlotCam.SetActive(true);
        isAddCoins = false;
        this.scatterObject.SetActive(false);
        this.coinShower.enableEmission = false;
        this.GenerateNewReels();
        int a = 0;
        while (a < this.reelInfo.Length)
        {
            this.RepositionReel(a, this.reelInfo[a].targetPosition);
            a++;
        }
        this.GenerateLineInfo();
        this.lineCount = this.linesInfo.lineInfo.Length;
        this.UpdateText();
        this.iconsSet = true;
        this.fadeValue = 1;
     

    }


    public void Start()
	{
        EnableCamera();

        if (PlayerPrefs.GetInt("LastWinSlot")>0)
        {
            LastWin.text = " WELCOME BACK!!!";
        }
        else
            LastWin.text = "WELCOME TO SLOT MACHINE!";

        Music.THIS.GetComponent<AudioSource>().Stop();
        Music.THIS.GetComponent<AudioSource>().loop = true;
        Music.THIS.GetComponent<AudioSource>().clip = Music.THIS.music[4];
        Music.THIS.GetComponent<AudioSource>().Play();

    }
    void EnableCamera()
    {
        float _check = (float)Screen.width / (float)Screen.height;

        float Check = (float)Mathf.Round(_check * 100) / 100f;
        if (Check == 2.16f || Check == 2.17f)
        {
            CoinPanel.transform.localPosition = new Vector3(25.25f, 12.48f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 12.72f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-24.39f, 12.52f, 0);
            PayTableObj.transform.localPosition = new Vector3(-24.39f, -12.16f, 0);


            SpinObj.transform.localPosition = new Vector3(22.5f, -12.11f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(14.57f, -12.11f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(14.57f, -12.11f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(14.57f, -12.11f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0, -12.14f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.15f, -11.4f, 0);
            LineObj.transform.localPosition = new Vector3(-14.46f, -11.4f, 0);


        }
        else if (Check == 2.06f)
        {
            CoinPanel.transform.localPosition = new Vector3(23.4f, 12.4f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 12.69f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-23.11f, 12.67f, 0);
            PayTableObj.transform.localPosition = new Vector3(-23.11f, -12.16f, 0);

            SpinObj.transform.localPosition = new Vector3(23.61f, -12.16f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(14.56f, -12.16f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(14.56f, -12.16f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(14.56f, -12.16f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -12.23f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.14f, -11.5f, 0);
            LineObj.transform.localPosition = new Vector3(-14.45f, -11.5f, 0);

        }
        else if (Check == 2.0f)
        {
            CoinPanel.transform.localPosition = new Vector3(23.48f, 12.9f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 13.13f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-23.74f, 12.99f, 0);
            PayTableObj.transform.localPosition = new Vector3(-23.74f, -12.5f, 0);

            SpinObj.transform.localPosition = new Vector3(23.27f, -12.49f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(14.54f, -12.49f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(14.54f, -12.49f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(14.54f, -12.49f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -12.51f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.14f, -11.75f, 0);
            LineObj.transform.localPosition = new Vector3(-14.46f, -11.75f, 0);

        }
        else if (Check == 1.78f)
        {
            CoinPanel.transform.localPosition = new Vector3(18.78f, 12.1f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 12.4f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-21.14f, 12.26f, 0);
            PayTableObj.transform.localPosition = new Vector3(-21.14f, -12.168f, 0);


            SpinObj.transform.localPosition = new Vector3(21.2f, -12.16f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(13.74f, -12.16f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(13.74f, -12.16f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(13.74f, -12.16f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -12.23f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.06f, -11.5f, 0);
            LineObj.transform.localPosition = new Vector3(-14.4f, -11.5f, 0);
        }
        else if (Check == 1.67f)
        {
            CoinPanel.transform.localPosition = new Vector3(18.09f, 12.56f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 12.8f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-20.54f, 12.71f, 0);
            PayTableObj.transform.localPosition = new Vector3(-20.54f, -12.16f, 0);


            SpinObj.transform.localPosition = new Vector3(20.62f, -12.21f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(14.53f, -12.21f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(14.53f, -12.21f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(14.53f, -12.21f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -12.25f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.12f, -11.5f, 0);
            LineObj.transform.localPosition = new Vector3(-14.4f, -11.5f, 0);
        }
        else if (Check == 1.5f)
        {
            CoinPanel.transform.localPosition = new Vector3(18.24f, 14.31f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 14.53f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-20.99f, 14.35f, 0);
            PayTableObj.transform.localPosition = new Vector3(-20.99f, -12.95f, 0);


            SpinObj.transform.localPosition = new Vector3(20.92f, -12.95f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(14.65f, -12.95f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(14.65f, -12.95f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(14.65f, -12.95f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -13f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.12f, -12.27f, 0);
            LineObj.transform.localPosition = new Vector3(-14.4f, -12.27f, 0);
        }
        else if (Check == 1.43f)
        {
            CoinPanel.transform.localPosition = new Vector3(15.16f, 12.91f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 13.17f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-18.75f, 12.99f, 0);
            PayTableObj.transform.localPosition = new Vector3(-18.75f, -12.46f, 0);


            SpinObj.transform.localPosition = new Vector3(18.2f, -12.46f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(12.43f, -12.46f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(12.43f, -12.46f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(12.43f, -12.46f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -12.53f, 0);
            BetLineObj.transform.localPosition = new Vector3(-7.64f, -11.79f, 0);
            LineObj.transform.localPosition = new Vector3(-13.39f, -11.79f, 0);
        }
        else if (Check == 1.33f)
        {
            CoinPanel.transform.localPosition = new Vector3(17.64f, 15.83f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 16.13f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-21.38f, 16.05f, 0);
            PayTableObj.transform.localPosition = new Vector3(-21.38f, -14.09f, 0);


            SpinObj.transform.localPosition = new Vector3(20.66f, -14.09f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(13.74f, -14.09f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(13.74f, -14.09f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(13.74f, -14.09f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -14.17f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.25f, -13.44f, 0);
            LineObj.transform.localPosition = new Vector3(-14.4f, -13.44f, 0);
        }
        else
        {
            CoinPanel.transform.localPosition = new Vector3(18.78f, 12.1f, 0);
            Header.transform.localPosition = new Vector3(1.7f, 12.4f, -1.2f);
            LobbyObj.transform.localPosition = new Vector3(-22.76f, 12.26f, 0);
            PayTableObj.transform.localPosition = new Vector3(-22.76f, -12.45f, 0);


            SpinObj.transform.localPosition = new Vector3(23.61f, -12.16f, 0);
            AutoSpinObj.transform.localPosition = new Vector3(14.56f, -12.16f, -1.2f);
            StopAutoObj.transform.localPosition = new Vector3(14.56f, -12.16f, -1.2f);
            AutoSpinEffectObj.transform.localPosition = new Vector3(14.56f, -12.16f, -1.2f);
            TotalBetObj.transform.localPosition = new Vector3(0f, -12.23f, 0);
            BetLineObj.transform.localPosition = new Vector3(-8.14f, -11.5f, 0);
            LineObj.transform.localPosition = new Vector3(-14.45f, -11.5f, 0);
        }
    }

    public virtual void Update()
    {

        CoinsTxt.text = DataManager.Instance.Coins.ToString("n0");
        DataManager.Instance.AddFluxCoins((int)this.totalPayout);
		if (DataManager.Instance.FluxCoins > DataManager.Instance.Coins)
			DataManager.Instance.ResetFluxCoins (); 
		picks1 = picks;
        if (this.prevIconCount != this.iconsPerReel)
        {
            this.GenerateNewReels();
        }
        if (this.generatingLine)
        {
            this.GenerateNewLine();
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (!this.inBonusGame)
            {
                this.Click(Input.mousePosition);
            }
            if (this.inBonusGame && (this.picks > 0))
            {
            }
        }
        if ((Input.touchCount > 0) && !this.touching)
        {
            if ((Input.GetTouch(0).phase == TouchPhase.Began) && !this.touching)
            {
                this.touching = true;
                if (!this.inBonusGame)
                {
                    this.Click(Input.GetTouch(0).position);
                }
                if (this.inBonusGame && (this.picks > 0))
                {
                }
            }
        }
        if ((((TouchPhase) Input.touchCount) == TouchPhase.Ended) || (((TouchPhase) Input.touchCount) == TouchPhase.Canceled))
        {
            if (this.touching)
            {
                this.touching = false;
            }
        }

        if (this.inBonusGame)
        {
        }
        if (this.spinning)
        {
            this.curSpinSpeed = Mathf.Lerp(this.curSpinSpeed, this.spinSpeed, Time.deltaTime);
            int i = 0;
            while (i < 5)
            {
                if (this.reelInfo[i].spinning)
                {

                    {
                        float _76 = Mathf.MoveTowards(this.reelInfo[i].reel.transform.position.y, this.reelInfo[i].targetPosition - this.reboundAmount, this.curSpinSpeed * Time.deltaTime);
                        Vector3 _77 = this.reelInfo[i].reel.transform.position;
                        _77.y = _76;
                        this.reelInfo[i].reel.transform.position = _77;
                    }
                }
                if (this.reelInfo[i].reel.transform.position.y == (this.reelInfo[i].targetPosition - this.reboundAmount))
                {
                    if (this.reelInfo[i].spinning)
                    {
                        this.StopReel(i);
                    }
                }
                i++;
            }
        }
        if (this.displayWinningEffects)
        {
            if(isAddCoins)
            {
                this.coinShower.enableEmission = true;
                FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.CoinPush);
                FXSound.THIS.fxSound.loop = true;
                isAddCoins = false;
            }
            this.effectsTimer = this.effectsTimer + Time.deltaTime;
            foreach (LineInfo line in this.linesInfo.lineInfo)
            {
                if (line.winner)
                {
                    if ((this.effectsTimer < 0.75f) && !line.lineParent.activeSelf)
                    {
                        line.lineParent.SetActive(true);
                    }
                    if (this.effectsTimer > 0.75f)
                    {
                        if (line.lineParent.activeSelf)
                        {
                            line.lineParent.SetActive(false);
                        }
                        if (this.effectsTimer >= 1)
                        {
                            this.effectsTimer = 0f;
                        }
                    }
                }
            }
        }
        if (this.scattersLeft > 0)
        {
            if (this.scatterCountObject.text != this.scattersLeft.ToString())
            {
                this.scatterCountObject.text = this.scattersLeft.ToString();
            }
            if (!this.inBonusGame && (this.tempBonusState == 0))
            {
                this.EngageScatters();
            }
        }
		if (this.isAutoSpin) {
			// Auto Spin
				EngageScatters1 ();

		}
        int r = 0;
        while (r < this.reelInfo.Length)
        {
            if (!this.reelInfo[r].spinning)
            {
                if (this.reelInfo[r].reel.transform.position.y < this.reelInfo[r].targetPosition)
                {

                    {
                        float _78 = Mathf.Lerp(this.reelInfo[r].reel.transform.position.y, this.reelInfo[r].targetPosition, this.reboundSpeed * Time.deltaTime);
                        Vector3 _79 = this.reelInfo[r].reel.transform.position;
                        _79.y = _78;
                        this.reelInfo[r].reel.transform.position = _79;
                    }
                }
            }
            r++;
        }
    }

    public virtual IEnumerator CheckForAnimatedIcons(int r, int s)
    {
        SlotInfo info = this.reelInfo[r].slotOrder[s];
        if (info.canAnimate)
        {
            if (this.iconInfo[info.ID].spriteAnimation.Length > 0)
            {
                int i = 0;
                while (i < this.iconInfo[info.ID].spriteAnimation.Length)
                {
                    if (this.iconInfo[info.ID].spriteAnimation[i] != null)
                    {
                        if (info.sprite)
                        {
                            info.sprite.GetComponent<SpriteRenderer>().sprite = this.iconInfo[info.ID].spriteAnimation[i];
                        }
                    }
                    if (!this.spinning)
                    {
                        yield return new WaitForSeconds(this.iconInfo[info.ID].animatedFramesPerSecond);
                        if (!this.spinning)
                        {
                            if (i == (this.iconInfo[info.ID].spriteAnimation.Length - 1))
                            {
                                info.sprite.GetComponent<SpriteRenderer>().sprite = this.iconInfo[info.ID].spriteAnimation[0];
                                info.canAnimate = false;
                            }
                        }
                    }
                    i++;
                }
            }
        }
    }

    public virtual void UpdateText()
    {
        foreach (DisplayInfo info in this.displayInfo)
        {
            this.gameObject.SendMessage("Update" + info.functionType.ToString(), info.textObject);
        }
    }

    public virtual void UpdateLineCount(TextMesh text)
    {
        text.text = this.lineCount.ToString();
    }

    public virtual void UpdateBetAmount(TextMesh text)
    {
		text.text = this.betAmounts[(int)this.currentBet].ToString();
    }

    public virtual void Lobby()
	{
        SlotCam.SetActive(false);
        MusicController.Music.audiosource.enabled = true;
        if (LoadingAnimator.gameObject.activeSelf)
        {
            LoadingAnimator.SetTrigger("LoadingOut");
        }
    }

    public virtual void PayTable()
    {
        Debug.Log(" PayTable Clicked");
        this.DarkenButtons();
        this.showPayTable = true;
        payTableImage.SetActive(true);
        if (PayTableAnimator.gameObject.activeSelf)
        {
            PayTableAnimator.SetTrigger("PayOn");
        }
    }
	public void Click()
	{
		FXSound.THIS.fxSound.PlayOneShot (FXSound.THIS.ButtonClick);
	}


    public virtual void UpdateTotalWin(TextMesh text)
    {
        if (this.totalPayout == 0)
        {
            text.text = "";
        }
        else
        {
            text.text = this.totalPayout.ToString();
            PlayerPrefs.SetInt("LastWinSlot", (int)bonusWinnings);
            PlayerPrefs.SetInt("LastWinSlot", PlayerPrefs.GetInt("LastWinSlot") + (int)totalPayout);
            PlayerPrefs.Save();
        }
    }

    public virtual void UpdateTotalBet(TextMesh text)
    {
		float totalBet = this.lineCount * this.betAmounts[(int)this.currentBet];
        text.text = totalBet.ToString();
    }

    public virtual void DarkenButtons()
    {

        foreach (ButtonInfo button in this.buttonInfo)
        {

            if (button.sprite != null && button.functionType != ButtonInfo.FunctionType.CloseButton)
            {
                if (button.sprite.GetComponent<SpriteRenderer>().material.color != Color.gray)
                {
                    button.sprite.GetComponent<SpriteRenderer>().material.color = Color.gray;
                }
            }

        }
    }

    public virtual void LightenButtons()
    {
        foreach (ButtonInfo button in this.buttonInfo)
        {
            if (button.sprite != null)
            {
                if (button.sprite.GetComponent<SpriteRenderer>().material.color != Color.white)
                {
                    button.sprite.GetComponent<SpriteRenderer>().material.color = Color.white;
                }
            }
        }
    }

    public virtual void Click(Vector3 position)
    {

        RaycastHit hit = default(RaycastHit);
        Ray ray = this.GetComponent<Camera>().ScreenPointToRay(position);
        if (Physics.Raycast(ray, out hit, 100))
        {
            foreach (ButtonInfo button in this.buttonInfo)
            {
                if (button.sprite != null && (button.sprite.GetComponent<SpriteRenderer>().material.color != Color.gray))
                { 
                    if (hit.transform == button.sprite.transform)
                    {
						FXSound.THIS.fxSound.PlayOneShot (FXSound.THIS.ButtonClick);
						if (button.functionType == ButtonInfo.FunctionType.Spin)
                        {
                            if (this.scattersLeft == 0)
                            {
								this.gameObject.SendMessage(button.functionType.ToString(), this.lineCount * this.betAmounts[(int)this.currentBet]);
                            }
                        }
                        else
                        {
                            this.Invoke(button.functionType.ToString(), 0f);
                        }
                    }
                }
            }
        }
    }

    public virtual void GenerateNewReels()
    {
        this.StorePreviousFaceIcons();
        this.RemovePreviousReels();
        this.UpdateAmountOfReels();
        this.UpdateIconsPerReel();
        this.PopulateFaceIcons();
    }
	public virtual void AutoSpin()
	{
		if (!this.isAutoSpin&&!this.spinning) {
			this.isAutoSpin = true;
			AutoSpinEffect.SetActive (true);
			Auto.SetActive (false);
			Stop.SetActive (true);
		} else if (this.isAutoSpin&&!this.spinning)
		{
			this.isAutoSpin = false;
			AutoSpinEffect.SetActive (false);
			Auto.SetActive (true);
			Stop.SetActive (false);
		}
			
	}
	public virtual void StopAuto()
	{
		if (!this.isAutoSpin&&!this.spinning) {
			this.isAutoSpin = true;
			AutoSpinEffect.SetActive (true);
			Auto.SetActive (false);
			Stop.SetActive (true);
		} else if (this.isAutoSpin&&!this.spinning)
		{
			this.isAutoSpin = false;
			AutoSpinEffect.SetActive (false);
			Auto.SetActive (true);
			Stop.SetActive (false);
		}

	}
    public virtual void CloseButton()
    {
        Debug.Log("Close PayTable Clicked");
        this.LightenButtons();
        if (PayTableAnimator.gameObject.activeSelf)
        {
            PayTableAnimator.SetTrigger("PayOff");
        }
    }

    public virtual void MaxBet()
    {
        if ((!this.spinning && !this.inBonusGame) && !this.scatterSpinning)
        {
            int maxedBet = this.GetLargestBet();
			if (maxedBet == 250000)
            {
                return;
            }
            this.lineCount = this.linesInfo.lineInfo.Length;
            this.currentBet = maxedBet;
			this.Spin(this.lineCount * this.betAmounts[(int)this.currentBet]);
        }
    }

    public virtual int GetLargestBet()
    {
        int i = this.betAmounts.Length - 1;
        while (i >= 0)
        {
			if (DataManager.Instance.Coins >= (this.lineCount * this.betAmounts[i]))
            {
                return i;
            }
            i--;
        }
		return 250000;
    }

	public virtual void Spin(float Deduction)
	{
		if (((!this.spinning && !this.inBonusGame) && !this.scatterSpinning))
		{
			if (Deduction <= DataManager.Instance.Coins )
			{
                DataManager.Instance.RemoveCoins((int)Deduction);
                PlayerPrefs.Save();
                PlayerPrefs.SetInt("SpinTimes", PlayerPrefs.GetInt("SpinTimes") + 1);
                isAddCoins = false;
                //--Last win--------------
                if (PlayerPrefs.GetInt("LastWinSlot") > 0)
                    LastWin.text = "YOUR LAST WIN: " + PlayerPrefs.GetInt("LastWinSlot").ToString(("n0"));
                //------------------------
                this.effectsTimer = 0;
				this.totalPayout = 0f;
				this.displayWinningEffects = false;
				this.coinShower.enableEmission = false;
				this.DarkenButtons();
				this.UpdateText();
				FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Spin);
				this.GenerateNewReels();
				this.CalculatePayout();
				this.spinning = true;
				int i = 0;
				while (i < 5)
				{
					this.reelInfo[i].spinning = true;
					FXSound.THIS.fxSound.GetComponent<AudioSource> ().volume = 1.0f;
					FXSound.THIS.fxSound.GetComponent<AudioSource> ().rolloffMode= (AudioRolloffMode) 1;
					FXSound.THIS.fxSound.GetComponent<AudioSource> ().spatialBlend= 0;
					i++;
				}
			}
		}
	}

    public virtual void StorePreviousFaceIcons()
    {
        System.Array.Resize<int>(ref this.prevFaceIcons, this.reelInfo.Length * 3);
        if (this.iconsSet)
        {
            this.prevFaceIcons = this.faceIcons;
        }
    }

    public virtual void PopulateFaceIcons()
    {
        System.Array.Resize<int>(ref this.faceIcons, this.reelInfo.Length * 3);
        System.Array.Resize<int>(ref this.faceSprites, this.reelInfo.Length * 3);
        int a = 0;
        while (a < this.reelInfo.Length)
        {
            int extraIcons = a * this.iconsPerReelDifference;
            this.faceIcons[a * 3] = this.reelInfo[a].slotOrder[(this.iconsPerReel + extraIcons) - 1].ID;
            this.faceSprites[a * 3] = (this.iconsPerReel + extraIcons) - 1;
            this.faceIcons[(a * 3) + 1] = this.reelInfo[a].slotOrder[(this.iconsPerReel + extraIcons) - 2].ID;
            this.faceSprites[(a * 3) + 1] = (this.iconsPerReel + extraIcons) - 2;
            this.faceIcons[(a * 3) + 2] = this.reelInfo[a].slotOrder[(this.iconsPerReel + extraIcons) - 3].ID;
            this.faceSprites[(a * 3) + 2] = (this.iconsPerReel + extraIcons) - 3;
            a++;
        }
    }

    public virtual void CalculatePayout()
    {
        int scatterCount = 0;
        int bonusCount = 0;
        this.tempBonusState = 0;
        foreach (LineInfo line in this.linesInfo.lineInfo)
        {
            line.lineParent.SetActive(false);
            line.winningValue = 0;
            line.winner = false;
        }
        int s = 0;
        while (s < this.faceIcons.Length)
        {
			if (this.iconInfo[this.faceIcons[s]].iconType ==  IconInfo.IconType.Scatter)
            {
                scatterCount = scatterCount + 1;
            }
            if (s == (this.faceIcons.Length - 1))
            {
                if (scatterCount >= 3)
                {
                    int scatterMultiplier = scatterCount - 2;
                    this.tempScatter = scatterCount * scatterMultiplier;
                }
            }
            s++;
        }
        int bo = 0;
        while (bo < this.faceIcons.Length)
        {
            if (this.iconInfo[this.faceIcons[bo]].iconType == IconInfo.IconType.Bonus)
            {
                bonusCount = bonusCount + 1;
            }
            if (bo == (this.faceIcons.Length - 1))
            {
                if (bonusCount >= 3)
                {
                    this.tempBonusState = 3;
                    Debug.Log("_____________________________________BONUS GAME");
                }
            }
            bo++;
        }
        int a = 0;
        while (a < this.lineCount)
        {
            PayoutInfo payoutIcon = new PayoutInfo();
            if (this.payoutOrder == (PayoutOrder) 0)
            {
				if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[0]]].iconType !=  IconInfo.IconType.Wild)
                {
                    payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[0]];
                }
				if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[0]]].iconType ==  IconInfo.IconType.Wild)
                {
                    int wB = 1;
                    while (wB < 5)
                    {
						if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wB]]].iconType ==  IconInfo.IconType.Normal)
                        {
                            payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wB]];
                            wB = 4;
                            break;
                        }
                        if (wB < 4)
                        {
							if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wB]]].iconType ==  IconInfo.IconType.Wild)
                            {
                                goto Label_for_45;
                            }
                        }
                        if (wB == 4)
                        {
							if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wB]]].iconType ==  IconInfo.IconType.Wild)
                            {
                                payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[0]];
                                break;
                            }
                        }
						if ((this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wB]]].iconType ==  IconInfo.IconType.Bonus) || (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wB]]].iconType ==  IconInfo.IconType.Scatter))
                        {
                            payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[0]];
                            wB = 4;
                            break;
                        }
                        Label_for_45:
                        wB++;
                    }
                }
                int b = 0;
                while (b < 5)
                {
                    payoutIcon.amount = payoutIcon.amount + 1;
                    if (this.payoutOrder == (PayoutOrder) 0)
                    {
                        if (b < 4)
                        {
							if ((payoutIcon.ID != this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[b + 1]]) && (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[b + 1]]].iconType !=  IconInfo.IconType.Wild))
                            {
                                int Aamount = b + 1;
                                int AlIne = a + 1;
                                b = 4;
                                break;
                            }
                        }
                    }
                    b++;
                }
            }
            if (this.payoutOrder == (PayoutOrder) 1)
            {
				if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[4]]].iconType !=  IconInfo.IconType.Wild)
                {
                    payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[4]];
                }
				if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[4]]].iconType ==  IconInfo.IconType.Wild)
                {
                    int wC = 3;
                    while (wC > -1)
                    {
						if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wC]]].iconType ==  IconInfo.IconType.Normal)
                        {
                            payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wC]];
                            wC = 0;
                            break;
                        }
                        if (wC > 0)
                        {
							if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wC]]].iconType ==  IconInfo.IconType.Wild)
                            {
                                goto Label_for_47;
                            }
                        }
                        if (wC == 0)
                        {
							if (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wC]]].iconType ==  IconInfo.IconType.Wild)
                            {
                                payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[4]];
                                break;
                            }
                        }
						if ((this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wC]]].iconType ==  IconInfo.IconType.Bonus) || (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[wC]]].iconType ==  IconInfo.IconType.Scatter))
                        {
                            payoutIcon.ID = this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[4]];
                            wC = 0;
                            break;
                        }
                        Label_for_47:
                        wC--;
                    }
                }
                int c = 4;
                while (c > -1)
                {
                    payoutIcon.amount = payoutIcon.amount + 1;
                    if (c > 0)
                    {
						if ((payoutIcon.ID != this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[c - 1]]) && (this.iconInfo[this.faceIcons[this.linesInfo.lineInfo[a].lineNumbers[c - 1]]].iconType !=  IconInfo.IconType.Wild))
                        {
                            int Damount = 5 - c;
                            int DlIne = a + 1;
                            c = 0;
                            break;
                        }
                    }
                    c--;
                }
            }
            if (this.iconInfo[payoutIcon.ID].xTwo > 0)
            {
				if ((this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Normal) || (this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Wild))
                {
					this.linesInfo.lineInfo[a].winningValue = this.iconInfo[payoutIcon.ID].xTwo * this.betAmounts[(int)this.currentBet];
                    this.linesInfo.lineInfo[a].winner = true;
                    this.linesInfo.lineInfo[a].winningIconIDs = new int[2];
                    if (this.payoutOrder == (PayoutOrder) 0)
                    {
                        this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.firstReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.secondReel;
                    }
                    if (this.payoutOrder == (PayoutOrder) 1)
                    {
                        this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.fifthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.forthReel;
                    }
                }
            }
            if (payoutIcon.amount == 3)
            {
				if ((this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Normal) || (this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Wild))
                {
					this.linesInfo.lineInfo[a].winningValue = this.iconInfo[payoutIcon.ID].xThree * this.betAmounts[(int)this.currentBet];
                    this.linesInfo.lineInfo[a].winner = true;
                    this.linesInfo.lineInfo[a].winningIconIDs = new int[3];
                    if (this.payoutOrder == (PayoutOrder) 0)
                    {
                        this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.firstReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.secondReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[2] = (int) this.linesInfo.lineInfo[a].LineNumbers.thirdReel;
                    }
                    if (this.payoutOrder == (PayoutOrder) 1)
                    {
                        this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.fifthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.forthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[2] = (int) this.linesInfo.lineInfo[a].LineNumbers.thirdReel;
                    }
                }
            }
            if (payoutIcon.amount == 4)
            {
				if ((this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Normal) || (this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Wild))
                {
					this.linesInfo.lineInfo[a].winningValue = this.iconInfo[payoutIcon.ID].xFour * this.betAmounts[(int)this.currentBet];
                    this.linesInfo.lineInfo[a].winner = true;
                    this.linesInfo.lineInfo[a].winningIconIDs = new int[4];
                    if (this.payoutOrder == (PayoutOrder) 0)
                    {
                        this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.firstReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.secondReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[2] = (int) this.linesInfo.lineInfo[a].LineNumbers.thirdReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[3] = (int) this.linesInfo.lineInfo[a].LineNumbers.forthReel;
                    }
                    if (this.payoutOrder == (PayoutOrder) 1)
                    {
                        this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.fifthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.forthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[2] = (int) this.linesInfo.lineInfo[a].LineNumbers.thirdReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[3] = (int) this.linesInfo.lineInfo[a].LineNumbers.secondReel;
                    }
                }
            }
            if (payoutIcon.amount == 5)
            {
				if ((this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Normal) || (this.iconInfo[payoutIcon.ID].iconType ==  IconInfo.IconType.Wild))
                {
					this.linesInfo.lineInfo[a].winningValue = this.iconInfo[payoutIcon.ID].xFive * this.betAmounts[(int)this.currentBet];
                    this.linesInfo.lineInfo[a].winner = true;
                    this.linesInfo.lineInfo[a].winningIconIDs = new int[5];
                    if (this.payoutOrder == (PayoutOrder) 0)
                    {
                        this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.firstReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.secondReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[2] = (int) this.linesInfo.lineInfo[a].LineNumbers.thirdReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[3] = (int) this.linesInfo.lineInfo[a].LineNumbers.forthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[4] = (int) this.linesInfo.lineInfo[a].LineNumbers.fifthReel;
                    }
                    if (this.payoutOrder == (PayoutOrder) 1)
                    {
                        this.linesInfo.lineInfo[a].winningIconIDs[0] = (int) this.linesInfo.lineInfo[a].LineNumbers.fifthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[1] = (int) this.linesInfo.lineInfo[a].LineNumbers.forthReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[2] = (int) this.linesInfo.lineInfo[a].LineNumbers.thirdReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[3] = (int) this.linesInfo.lineInfo[a].LineNumbers.secondReel;
                        this.linesInfo.lineInfo[a].winningIconIDs[4] = (int) this.linesInfo.lineInfo[a].LineNumbers.firstReel;
                    }
                }
            }
            a++;
        }
    }

    public virtual void StopReel(int key)
    {
        float payout = 0.0f;
        this.reelInfo[key].spinning = false;
		FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.ReelStop);
        if (key == 4)
        {
            this.spinning = false;
            this.curSpinSpeed = 0;
			FXSound.THIS.fxSound.Stop();
			FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.ReelStop);

            foreach (LineInfo line in this.linesInfo.lineInfo)
            {
                if (line.winningValue > 0f)
                {
                    payout = payout + line.winningValue;
                }
                if (line.winner)
                {
                    int i = 0;
                    while (i < line.winningIconIDs.Length)
                    {
                        if (this.payoutOrder == (PayoutOrder) 0)
                        {
                            this.reelInfo[i].slotOrder[(this.reelInfo[i].slotOrder.Length - 1) - line.winningIconIDs[i]].canAnimate = true;
                        }
                        if (this.payoutOrder == (PayoutOrder) 1)
                        {
                            this.reelInfo[4 - i].slotOrder[(this.reelInfo[4 - i].slotOrder.Length - 1) - line.winningIconIDs[i]].canAnimate = true;
                        }
                        i++;
                    }
                    line.winningIconIDs = new int[0];
                }
            }
            if (this.tempScatter > 0)
            {
                this.scatterTimer = 2;
                this.scattersLeft = this.scattersLeft + this.tempScatter;
                this.tempScatter = 0;
                if ((this.tempBonusState == 0) )
                {
					FXSound.THIS.fxSound.PlayOneShot (FXSound.THIS.Scatter);
                    PlayerPrefs.SetInt("ScatterGames", PlayerPrefs.GetInt("ScatterGames") + 1);
                }
            }
            // if (this.tempBonusState > 2)
            // {
            //     this.picks = this.tempBonusState;
            //     this.StartCoroutine(this.BonusFade());
            // }
            if (payout > 0f)
            {
                //                this.AddCoins(payout, true);
                isAddCoins = true;
                DataManager.Instance.AddCoins((int)payout);
				this.totalPayout = this.totalPayout + payout;
                this.displayWinningEffects = true;
            }
            if (this.scattersLeft == 0)
            {
                this.LightenButtons();
            }
            int a = 0;
            while (a < this.reelInfo.Length)
            {
                this.StartCoroutine(this.CheckForAnimatedIcons(a, this.reelInfo[a].slotOrder.Length - 1));
                this.StartCoroutine(this.CheckForAnimatedIcons(a, this.reelInfo[a].slotOrder.Length - 2));
                this.StartCoroutine(this.CheckForAnimatedIcons(a, this.reelInfo[a].slotOrder.Length - 3));
                a++;
            }
            this.UpdateText();
        }
    }

    public virtual void EngageScatters()
    {
        if (!this.scatterObject.activeSelf)
        {
            this.scatterObject.SetActive(true);
        }
        if (!this.spinning && !this.inBonusGame)
        {
            this.scatterTimer = this.scatterTimer - Time.deltaTime;
        }
        if (this.spinning && (this.scatterTimer != 3))
        {
            this.scatterTimer = 3;
        }
        if (this.scatterTimer < 0)
        {
            this.Spin(0f);
            this.scatterTimer = 3;
            this.scattersLeft = this.scattersLeft - 1;
            if (this.scattersLeft == 0)
            {
                this.scatterObject.SetActive(false);
            }
        }
    }
	public virtual void EngageScatters1()
	{
		if (!this.spinning && !this.inBonusGame)
		{
			this.autoTimer = this.autoTimer - Time.deltaTime;
		}
		if (this.spinning && (this.autoTimer != 3))
		{
			this.autoTimer = 3;
		}
		if (this.autoTimer < 0)
		{
			this.AutoSpinMehod(this.lineCount * this.betAmounts[(int)this.currentBet]);
		}
	}

	//----------\
	public virtual void AutoSpinMehod(float Deduction)
	{
		if (((!this.spinning && !this.inBonusGame) && !this.scatterSpinning) )
		{
			if (Deduction <= DataManager.Instance.Coins )
			{
                PlayerPrefs.SetInt("SpinTimes", PlayerPrefs.GetInt("SpinTimes") + 1);
                isAddCoins = false;
                //--Last win--------------
                if (PlayerPrefs.GetInt("LastWinSlot") > 0)
                    LastWin.text = "YOUR LAST WIN: " + PlayerPrefs.GetInt("LastWinSlot").ToString(("n0"));
                //------------------------
                this.effectsTimer = 0;
				this.totalPayout = 0f;
				this.displayWinningEffects = false;
				this.coinShower.enableEmission = false;
				this.DarkenButtons();
				this.UpdateText();
				FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Spin);
				DataManager.Instance.RemoveCoins((int)Deduction);
				this.GenerateNewReels();
				this.CalculatePayout();
				this.spinning = true;
				int i = 0;
				while (i < 5)
				{
					this.reelInfo[i].spinning = true;
					FXSound.THIS.fxSound.GetComponent<AudioSource> ().volume = 1.0f;
					FXSound.THIS.fxSound.GetComponent<AudioSource> ().rolloffMode= (AudioRolloffMode) 1;
					FXSound.THIS.fxSound.GetComponent<AudioSource> ().spatialBlend= 0;
					i++;
				}
			}
		}
	}
	//----------


  //   public virtual IEnumerator BonusFade()
  //   {
  //       Debug.Log("__________________________________________________________________BONUS STARTED");
  //       FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.BonusWorld);
  //       this.inBonusGame = true;
  //       if (this.scatterObject.activeSelf)
  //       {
  //           this.scatterObject.SetActive(false);
  //       }
  //       this.bonusInfo.bonusAmountText.text = this.tempBonusState.ToString();
  //       this.bonusInfo.bonusInfoParent.SetActive(true);
  //       this.RandomArrangement(this.bonusInfo.winningAmounts);
  //
  //       this.bonusInfo.bonusText.GetComponent<Animation>().Play("ShowBonusWord");
  //       yield return new WaitForSeconds(3.1f);
  //       this.tempBonusState = 0;
  //       this.displayWinningEffects = false;
  //       this.coinShower.enableEmission = false;
  //       int a = 0;
  //       while (a < this.linesInfo.lineInfo.Length)
  //       {
  //           this.linesInfo.lineInfo[a].lineParent.SetActive(false);
  //           a++;
  //       }
  //       this.fadeValue = 0;
  //       foreach (ReelInfo reel in this.reelInfo)
  //       {
  //           reel.reel.SetActive(false);
  //       }
		// FXSound.THIS.fxSound.PlayOneShot (FXSound.THIS.BonusStart);
  //
  //       this.bonusInfo.bonusBackground.GetComponent<Animation>().Play("BonusStart");
  //
		// Music.THIS.GetComponent<AudioSource> ().Stop ();
		// Music.THIS.GetComponent<AudioSource> ().loop = true;
		// Music.THIS.GetComponent<AudioSource> ().clip = Music.THIS.music[2];
		// Music.THIS.GetComponent<AudioSource> ().Play ();
  //       int i = 0;
  //       while (i < this.bonusInfo.bonusItemInfo.Length)
  //       {
  //           this.bonusInfo.bonusItemInfo[i].@object.GetComponent<Renderer>().enabled = true;
  //           this.bonusInfo.bonusItemInfo[i].@object.GetComponent<Collider>().enabled = true;
		// 	this.bonusInfo.bonusItemInfo[i].@object.GetComponent<BonusObjectValue>().Value = this.bonusInfo.winningAmounts[i] * this.betAmounts[(int)this.currentBet];
  //           i++;
  //       }
  //
		// CanvasBonus.SetActive (true);
  //       PlayerPrefs.SetInt("BonusGames", PlayerPrefs.GetInt("BonusGames") + 1);
  //   }

    public virtual void RandomArrangement(float[] values)
    {
        int i = values.Length - 1;
        while (i > 0)
        {
            int r = Random.Range(0, i);
            float tmp = values[i];
            values[i] = values[r];
            values[r] = tmp;
            i--;
        }
    }

    public virtual IEnumerator CheckPicks()
    {
        if (this.picks == 0)
        {
            yield return new WaitForSeconds(1);
            // this.StartCoroutine(this.EndBonusGame());
        }
		DataManager.Instance.AddCoins((int)this.bonusWinnings);
		this.totalPayout = this.totalPayout + (int)this.bonusWinnings;
     
    }

//     public virtual IEnumerator EndBonusGame()
//     {
// 		
//         this.picks = 0;
//         this.bonusInfo.bonusInfoParent.SetActive(false);
//         int i = 0;
//         while (i < this.bonusInfo.bonusItemInfo.Length)
//         {
//             this.bonusInfo.bonusItemInfo[i].@object.GetComponent<Renderer>().enabled = false;
//             this.bonusInfo.bonusItemInfo[i].@object.GetComponent<Collider>().enabled = false;
// 			this.bonusInfo.bonusItemInfo[i].@object.GetComponent<BonusObjectValue>().displayValue = false;
//             i++;	
//         }
//         yield return new WaitForSeconds(2);
//         int j = 0;
//         while (j < this.bonusInfo.bonusItemInfo.Length)
//         {
//             this.bonusInfo.bonusItemInfo[j].@object.GetComponent<BonusObjectValue>().displayValue = false;
//             this.bonusInfo.bonusItemInfo[j].valueOpacity = 0;
//             j++;
//         }
//         if (this.bonusInfo.bonusBackground.GetComponent<Animation>()["BonusEnd"])
//         {
//             this.bonusInfo.bonusBackground.GetComponent<Animation>().Play("BonusEnd");
//         }
//
// 		FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.BonusResult);
//
// 		CanvasBonus.SetActive (false);
//         this.inBonusGame = false;
//         yield return new WaitForSeconds(1);
//         this.fadeValue = 1f;
//         this.displayWinningEffects = true;
//         this.bonusWinnings = 0;
//         this.UpdateText();
//         yield return new WaitForSeconds(1);
//
//         foreach (ReelInfo reel in this.reelInfo)
//         {
//             reel.reel.SetActive(true);
//         }
// 		Music.THIS.GetComponent<AudioSource> ().Stop ();
// 		Music.THIS.GetComponent<AudioSource> ().loop = true;
// 		Music.THIS.GetComponent<AudioSource> ().clip = Music.THIS.music[0];
// 		Music.THIS.GetComponent<AudioSource> ().Play ();
//
// //		this.displayWinningEffects = true;
//     }

    public virtual void IncreaseLines()
    {
        if ((!this.spinning && !this.inBonusGame) && !this.scatterSpinning)
        {
            if (this.lineCount < this.linesInfo.lineInfo.Length)
            {
                this.lineCount = this.lineCount + 1;
            }
            else
            {
                this.lineCount = 1;
            }
            this.UpdateText();
            this.SetVisuals();
        }
    }

    public virtual void DecreaseLines()
    {
        if ((!this.spinning && !this.inBonusGame) && !this.scatterSpinning)
        {
            if (this.lineCount > 1)
            {
                this.lineCount = this.lineCount - 1;
            }
            else
            {
                this.lineCount = this.linesInfo.lineInfo.Length;
            }
            this.UpdateText();
            this.SetVisuals();
        }
    }

    public virtual void SetVisuals()
    {
        this.displayWinningEffects = false;
        int a = 1;
        while (a < (this.linesInfo.lineInfo.Length + 1))
        {
            if (a <= this.lineCount)
            {
                this.linesInfo.lineInfo[a - 1].lineParent.SetActive(true);
            }
            if (a > this.lineCount)
            {
                this.linesInfo.lineInfo[a - 1].lineParent.SetActive(false);
            }
            a++;
        }
    }

    public virtual void IncreaseBet()
    {
        if ((!this.spinning && !this.inBonusGame) && !this.scatterSpinning)
        {
            if (this.currentBet == this.maxBet)
            {
                this.currentBet = 0;
            }
            else
            {
                this.currentBet = this.currentBet + 1;
            }
            this.UpdateText();
        }
    }

    public virtual void DecreaseBet()
    {
        if ((!this.spinning && !this.inBonusGame) && !this.scatterSpinning)
        {
            if (this.currentBet == 0)
            {
                this.currentBet = this.maxBet;
            }
            else
            {
                this.currentBet = this.currentBet - 1;
            }
            this.UpdateText();
        }
    }

    public virtual void GenerateLineInfo()
    {
        int a = 0;
        while (a < 5)
        {
            int b = 0;
            while (b < 3)
            {
                this.linePositions[(a * 3) + b] = this.reelInfo[a].slotOrder[(this.reelInfo[a].slotOrder.Length - 1) - b].sprite.transform.position + new Vector3(this.reelInfo[a].slotOrder[(this.reelInfo[a].slotOrder.Length - 1) - b].size.x / 2, 0, -0.5f);
                b++;
            }
            a++;
        }
        int i = 0;
        while (i < this.linesInfo.lineInfo.Length)
        {
            if (this.linesInfo.lineInfo[i].lineParent)
            {
                this.linesInfo.lineInfo[i].lineParent.SetActive(false);
                this.linesInfo.lineInfo[i].lineParent.GetComponent<Renderer>().material = new Material(this.linesInfo.lineShader);
                this.linesInfo.lineInfo[i].lineParent.GetComponent<Renderer>().sharedMaterial.color = this.linesInfo.lineInfo[i].thisColor;
                System.Array.Resize<int>(ref this.linesInfo.lineInfo[i].lineNumbers, 5);
                this.linesInfo.lineInfo[i].lineNumbers[0] = (int) this.linesInfo.lineInfo[i].LineNumbers.firstReel;
                this.linesInfo.lineInfo[i].lineNumbers[1] = (int) (this.linesInfo.lineInfo[i].LineNumbers.secondReel + 3);
                this.linesInfo.lineInfo[i].lineNumbers[2] = (int) (this.linesInfo.lineInfo[i].LineNumbers.thirdReel + 6);
                this.linesInfo.lineInfo[i].lineNumbers[3] = (int) (this.linesInfo.lineInfo[i].LineNumbers.forthReel + 9);
                this.linesInfo.lineInfo[i].lineNumbers[4] = (int) (this.linesInfo.lineInfo[i].LineNumbers.fifthReel + 12);
                if (this.linesInfo.lineInfo[i].lineBoxPosition.x > 0)
                {
                    this.ReverseLineVisuals(i);
                }
                else
                {
                    this.LineVisuals(i);
                }
            }
            i++;
        }
    }

    public virtual void GenerateNewLine()
    {
        if (!this.lines)
        {
            GameObject lineGrandparent = new GameObject();
            lineGrandparent.name = "LineManager";
            this.lines = lineGrandparent;
        }
        GameObject lineParent = new GameObject();
        lineParent.AddComponent<LineRenderer>();
        lineParent.name = "NewLine";
        int l = 0;
        while (l < 5)
        {
            GameObject lineChild = new GameObject();
            lineChild.AddComponent<LineRenderer>();
            lineChild.name = "Segment " + l.ToString();
            lineChild.transform.parent = lineParent.transform;
            l++;
        }
        lineParent.transform.parent = this.lines.transform;
        lineParent.SetActive(false);
        this.generatingLine = false;
    }

    public virtual void LineVisuals(int ID)
    {
        ((LineRenderer) this.linesInfo.lineInfo[ID].lineParent.GetComponent(typeof(LineRenderer))).SetPosition(0, this.linesInfo.lineInfo[ID].lineBoxPosition + new Vector3(0.5f, 0, -0.5f));
        ((LineRenderer) this.linesInfo.lineInfo[ID].lineParent.GetComponent(typeof(LineRenderer))).SetPosition(1, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[0]]);
        ((LineRenderer) this.linesInfo.lineInfo[ID].lineParent.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
        Transform parentObject = null;
        parentObject = this.linesInfo.lineInfo[ID].lineParent.transform;
        int i = 0;
        while (i < 5)
        {
            Transform child = null;
            child = this.linesInfo.lineInfo[ID].lineParent.transform.GetChild(i);
            child.GetComponent<Renderer>().material = parentObject.gameObject.GetComponent<Renderer>().sharedMaterial;
            ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetPosition(0, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[i]]);
            ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
            if (i < 4)
            {
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetPosition(1, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[i + 1]]);
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
            }
            else
            {
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetPosition(1, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[i]] + new Vector3(5, 0, 0));
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
            }
            parentObject = child;
            i++;
        }
    }

    public virtual void ReverseLineVisuals(int ID)
    {
        ((LineRenderer) this.linesInfo.lineInfo[ID].lineParent.GetComponent(typeof(LineRenderer))).SetPosition(0, this.linesInfo.lineInfo[ID].lineBoxPosition - new Vector3(0.5f, 0, 0.5f));
        ((LineRenderer) this.linesInfo.lineInfo[ID].lineParent.GetComponent(typeof(LineRenderer))).SetPosition(1, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[4]]);
        ((LineRenderer) this.linesInfo.lineInfo[ID].lineParent.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
        Transform parentObject = null;
        parentObject = this.linesInfo.lineInfo[ID].lineParent.transform;
        int i = 4;
        while (i > -1)
        {
            Transform child = null;
            child = this.linesInfo.lineInfo[ID].lineParent.transform.GetChild(i);
            child.GetComponent<Renderer>().sharedMaterial = parentObject.GetComponent<Renderer>().sharedMaterial;
            ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetPosition(0, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[i]]);
            ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
            if (i > 0)
            {
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetPosition(1, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[i - 1]]);
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
            }
            else
            {
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetPosition(1, this.linePositions[this.linesInfo.lineInfo[ID].lineNumbers[i]] - new Vector3(5, 0, 0));
                ((LineRenderer) child.GetComponent(typeof(LineRenderer))).SetWidth(this.linesInfo.lineWidth, this.linesInfo.lineWidth);
            }
            parentObject = child;
            i--;
        }
    }

    public virtual void UpdateAmountOfReels()
    {
		System.Array.Resize<ReelInfo>(ref this.reelInfo, 5);
        int i = 0;
        while (i < 5)
        {
            this.reelInfo[i] = new ReelInfo();
            this.reelInfo[i].reel = new GameObject();
            this.reelInfo[i].reel.name = "Reel " + i.ToString();
            this.reelInfo[i].reel.transform.parent = this.transform;
            i++;
        }
    }

    public virtual void UpdateIconsPerReel()
    {
        int randomIcon = 0;
        int a = 0;
        while (a < this.reelInfo.Length)
        {
            int extraIcons = a * this.iconsPerReelDifference;
            System.Array.Resize<SlotInfo>(ref this.reelInfo[a].slotOrder, this.iconsPerReel + extraIcons);
            int i = 0;
            while (i < (this.iconsPerReel + extraIcons))
            {
                this.reelInfo[a].slotOrder[i] = new SlotInfo();
                GameObject newSprite = new GameObject();
                newSprite.AddComponent<SpriteRenderer>();
                newSprite.name = "Slot " + i.ToString();
                this.reelInfo[a].slotOrder[i].sprite = newSprite;
                this.reelInfo[a].slotOrder[i].sprite.transform.localScale = new Vector3(this.iconSize, this.iconSize, 1);
                if (this.iconInfo.Length > 0)
                {
                    while (true)
                    {
                        randomIcon = Random.Range(0, this.iconInfo.Length - 1);
						float dividend = ((float) this.iconInfo[randomIcon].frequency) + 1;
                        float randomValue = Random.value;
                        if ((1 / dividend) > randomValue)
                        {
                            break;
                        }
                    }
                    if (!this.iconsSet)
                    {
                        this.reelInfo[a].slotOrder[i].ID = randomIcon;
                    }
                    if (this.iconsSet)
                    {
                        if (i < 3)
                        {
                            int row = 2 - i;
                            this.reelInfo[a].slotOrder[i].ID = this.prevFaceIcons[(a * 3) + row];
                        }
                        else
                        {
                            this.reelInfo[a].slotOrder[i].ID = randomIcon;
                        }
                    }
                    this.reelInfo[a].slotOrder[i].sprite.GetComponent<SpriteRenderer>().sprite = this.iconInfo[this.reelInfo[a].slotOrder[i].ID].sprite;
                    this.reelInfo[a].slotOrder[i].size = new Vector2(this.reelInfo[a].slotOrder[i].sprite.GetComponent<SpriteRenderer>().bounds.extents.x * 2, this.reelInfo[a].slotOrder[i].sprite.GetComponent<SpriteRenderer>().bounds.extents.y * 2);
                    this.reelInfo[a].slotOrder[i].sprite.transform.position = new Vector3((a * this.reelInfo[a].slotOrder[i].size.x) - (this.reelInfo[a].slotOrder[i].size.x * 2.5f), this.reelInfo[a].reel.transform.position.y + (i * this.reelInfo[a].slotOrder[i].size.y), 0);
                }
                newSprite.transform.parent = this.reelInfo[a].reel.transform;
                i++;
            }
            this.RepositionReel(a, -this.reelInfo[a].slotOrder[0].size.y);
            int offset = (this.iconsPerReel + extraIcons) - 2;
            this.reelInfo[a].targetPosition = this.reelInfo[a].slotOrder[0].size.y * -offset;
            a++;
        }
        this.prevIconCount = this.iconsPerReel;
    }

    public virtual void RepositionReel(int ID, float yPos)
    {

        {
            float _80 = yPos;
            Vector3 _81 = this.reelInfo[ID].reel.transform.position;
            _81.y = _80;
            this.reelInfo[ID].reel.transform.position = _81;
        }
    }

    public virtual void CreateLine()
    {
        this.generatingLine = true;
    }

    public virtual void RemovePreviousReels()
    {
        if (this.reelInfo.Length > 0)
        {
            foreach (ReelInfo info in this.reelInfo)
            {
                if (info.slotOrder.Length > 0)
                {
                    int i = 0;
                    while (i < info.slotOrder.Length)
                    {
                        UnityEngine.Object.DestroyImmediate(info.slotOrder[i].sprite);
                        i++;
                    }
                }
                UnityEngine.Object.DestroyImmediate(info.reel);
            }
        }
    }

    public virtual void OnGUI()
    {
        int l = 0;
        while (l < this.linesInfo.lineInfo.Length)
        {
            GUI.skin.label.alignment = TextAnchor.MiddleLeft;
            Vector3 screenPos = this.GetComponent<Camera>().WorldToScreenPoint(this.linesInfo.lineInfo[l].lineBoxPosition);
            Vector2 guiPos = new Vector2(screenPos.x, Screen.height - screenPos.y);
            GUI.color = this.linesInfo.lineInfo[l].thisColor;
            GUI.DrawTexture(new Rect(guiPos.x - (this.linesInfo.lineBoxSize.x / 2), guiPos.y - (this.linesInfo.lineBoxSize.y / 2), this.linesInfo.lineBoxSize.x, this.linesInfo.lineBoxSize.y), this.linesInfo.lineBlock);
            int thisLineNumber = l + 1;
            GUI.color = Color.black;
            GUI.skin.label.alignment = TextAnchor.MiddleCenter;
            GUI.skin.label.fontSize = (int) this.linesInfo.lineNumberSize;
            GUI.Label(new Rect(guiPos.x - 25, guiPos.y - 25, 50, 50), thisLineNumber.ToString());
            GUI.skin.label.fontSize = 0;
            l++;
        }
    }

    public SlotMachines()
    {
        this.betAmounts = new float[] { 1f, 2f, 3f, 5f, 7f, 10f, 20f, 30f, 40f, 60f, 80f, 100f, 150f, 300f, 500f, 1000f, 5000f, 10000f, 15000f, 20000f, 30000f, 50000f, 100000f, 150000f, 200000f, 250000f };
        this.linePositions = new Vector3[15];
        this.currentBet = 3f;
        this.fadeValue = 1;
    }

}