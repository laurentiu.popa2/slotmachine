using UnityEngine;
using System.Collections;


[System.Serializable]
public class ReelInfo : object
{
     //The reel object this belongs to
    public GameObject reel;
    //A list of icons on this reel in from center to bottom, top to center
    public SlotInfo[] slotOrder;
    //Determines whether or not this reel should be spinning
    public bool spinning;
    //The offset position this reel is going to spin to (HIDDEN IN THE INSPECTOR)
    [UnityEngine.HideInInspector]
    public float targetPosition;
}
//////////All the information needed for the icons position//////////
[System.Serializable]
public class SlotInfo : object
{
    public int ID;
    public GameObject sprite;
    public Vector2 size;
    public bool canAnimate;
    public bool animating;
    public bool collided;
}
//////////All the information needed for the icon//////////
[System.Serializable]
public class IconInfo : object
{
     //The name of this icon
    public string Name;
    public Sprite sprite;
    public Sprite[] spriteAnimation;
    public float animatedFramesPerSecond;
    public enum IconType
    {
        Normal = 0,
        Bonus = 1,
        Scatter = 2,
        Wild = 3
    }


    public IconInfo.IconType iconType;
    public enum Frequency
    {
        MostCommon = 0,
        Common = 1,
        LessCommon = 2,
        Rare = 3
    }


    public IconInfo.Frequency frequency;
    //How much is two icons on a line worth
    [UnityEngine.Range(0, 5)]
    public float xTwo;
    //How much is three icons on a line worth
    [UnityEngine.Range(0, 50)]
    public float xThree;
    //How much is four icons on a line worth
    [UnityEngine.Range(0, 250)]
    public float xFour;
    //How much is five icons on a line worth
    [UnityEngine.Range(0, 10000)]
    public float xFive;
}
//////////All the information needed for the lines//////////
[System.Serializable]
public class LineInfo : object
{
     //The positions in list that make up this line (REFER TO THE REFERENCE DIAGRAM PROVIDED)
    public LineNumbersInfo LineNumbers;
    //The position of the box that will display our line number
    public Vector3 lineBoxPosition;
    //Which line object that this line belongs to
    public GameObject lineParent;
    //Which color our line will be
    public Color thisColor;
    //How much we have won on this line (HIDDEN IN THE INSPECTOR)
    [UnityEngine.HideInInspector]
    public float winningValue;
    //Defines if we have won on this line (HIDDEN IN THE INSPECTOR)
    [UnityEngine.HideInInspector]
    public bool winner;
    [UnityEngine.HideInInspector]
    public Vector3[] anchorPositions;
    [UnityEngine.HideInInspector]
    public int[] lineNumbers;
    [UnityEngine.HideInInspector]
    public int winningIconID;
    [UnityEngine.HideInInspector]
    public int[] winningIconIDs;
}
[System.Serializable]
public class LineNumbersInfo : object
{
    public enum slot
    {
        Top = 0,
        Middle = 1,
        Bottom = 2
    }


    public LineNumbersInfo.slot firstReel;
    public LineNumbersInfo.slot secondReel;
    public LineNumbersInfo.slot thirdReel;
    public LineNumbersInfo.slot forthReel;
    public LineNumbersInfo.slot fifthReel;
}
[System.Serializable]
public class LinesInfo : object
{
    public Texture lineBlock;
    public Vector2 lineBoxSize;
    public float lineNumberSize;
    public Shader lineShader;
    public float lineWidth;
    public LineInfo[] lineInfo;
}
//////////All the information needed for the bonus items//////////
[System.Serializable]
public class BonusItemInfo : object
{
     //The object that we will be able to click
    public GameObject @object;
    //And the opacity of the worth value
    [UnityEngine.HideInInspector]
    public float valueOpacity;
}
[System.Serializable]
public class BonusInfo : object
{
    public GameObject bonusText;
    public GameObject bonusBackground;
    public GameObject bonusInfoParent;
    public TextMesh bonusAmountText;
    public Camera bonusCamera;
    public BonusItemInfo[] bonusItemInfo;
    public float[] winningAmounts;
    public SpriteRenderer[] fadeObjects;
}
//////////All the information needed for the rooms//////////
[System.Serializable]
public class LevelInfo : object
{
     //This picture that will be displayed on the main menu to represent this room
    public Texture Icon;
    public string sceneName;
    public int levelToUnlock;
    //To let us know if we have unlocked this level
    [UnityEngine.HideInInspector]
    public bool Locked;
}
//////////All the information needed for the audio//////////
//[System.Serializable]
//public class AudioInfo : object
//{
//     //The name of the audio clip
//    public string clipName;
//    //And what volume should we play it at
//    [UnityEngine.Range(0, 100)]
//    public float audioVolume;
//    //The audio clip itself
//    public AudioClip audioClip;
//}
[System.Serializable]
public class ButtonInfo : object
{
    public GameObject sprite;
    public enum FunctionType
    {
        Spin = 0,
        DecreaseLines = 1,
        IncreaseLines = 2,
        DecreaseBet = 3,
        IncreaseBet = 4,
        PayTable = 5,
        MaxBet = 6,
        AutoSpin = 7,
        Lobby = 8,
		StopAuto = 9,
        CloseButton = 10
    }


    public ButtonInfo.FunctionType functionType;
}
[System.Serializable]
public class DisplayInfo : object
{
    public TextMesh textObject;
    public enum FunctionType
    {
        LineCount = 0,
        BetAmount = 1,
        TotalBet = 2,
        TotalWin = 3
    }


    public DisplayInfo.FunctionType functionType;
}
[System.Serializable]
public class PayoutInfo : object
{
    public int amount;
    public int ID;
}