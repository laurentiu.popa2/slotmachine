using UnityEngine;
using System.Collections;

[System.Serializable]
public partial class BonusObjectValue : MonoBehaviour
{
    //////////Information is accessed and manipulated from the slot machine script//////////
    //How much this clickable bonus object is worth (HIDDEN IN THE INSPECTOR)
    //@HideInInspector
    public float Value;
    //Determines if we should display the value of the bonus object (HIDDEN IN THE INSPECTOR)
    //@HideInInspector
    public bool displayValue;
}