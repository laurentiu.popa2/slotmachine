﻿using UnityEngine;

public class FXSound : MonoBehaviour {
	public static FXSound THIS;
	[HideInInspector]
	public AudioSource fxSound;
	public AudioClip none;
	public AudioClip Spin;
	public AudioClip ReelStop;
	public AudioClip Scatter;
	public AudioClip BonusWorld;
	public AudioClip BonusStart;
	public AudioClip BonusAmbience;
	public AudioClip BonusPick;
	public AudioClip BonusResult;
	public AudioClip CoinPush;
	public AudioClip ButtonClick,ButtonBuy;
    public AudioClip Hit;
    public AudioClip Spin2;
    public AudioClip Scatter2;
    public AudioClip Drop;
    public AudioClip Line;
    public AudioClip Win2;
    public AudioClip Explosion;
    public AudioClip Intro3;
    public AudioClip Spin3;
    public AudioClip SpinLoop3;
    public AudioClip ReelStop3;
    public AudioClip Lose3;
    public AudioClip Beep3;
    public AudioClip Bet3;
    public AudioClip WinSmall;
    public AudioClip WinMedium;
    public AudioClip WinBig;
    public AudioClip WinSpecial;
    public AudioClip Pay3;
    public AudioClip EarnSmall;
    public AudioClip EarnBig;
    public AudioClip Bonus;
    public AudioClip SpinBonus;
    public AudioClip Impact;
    public AudioClip Camera;
    public AudioClip CallBackSuccess;
    public AudioClip OpenGift;
    public AudioClip OpenGift1;
    public AudioClip Click2;
    public AudioClip CountCoins;
    public AudioClip AchievementPopup;
    // Use this for initialization
    void Awake(){
		if (THIS == null) {
			THIS = this;
			DontDestroyOnLoad (gameObject);
		} else if (THIS != this) {
			Destroy (gameObject);
		}
		fxSound = GetComponent<AudioSource> ();
		gameObject.GetComponent<AudioSource> ().volume = PlayerPrefs.GetInt ("Sound");
	}
}
