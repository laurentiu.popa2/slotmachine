﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour {
	public static Music THIS;
	[HideInInspector]
	public AudioSource musicAudioSource;
	// Use this for initialization
	public AudioClip[] music;
	void Awake () {
		if (THIS == null) {
			THIS = this;
			DontDestroyOnLoad (gameObject);
		} else if (THIS != this) {
			Destroy (gameObject);
		}
		musicAudioSource = GetComponent<AudioSource> ();
		gameObject.GetComponent<AudioSource> ().volume = PlayerPrefs.GetInt ("Music");
	}
}
