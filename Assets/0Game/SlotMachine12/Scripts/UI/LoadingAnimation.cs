﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingAnimation : MonoBehaviour {

    // Use this for initialization
    IEnumerator AloadingEnd()
    {
        SceneManager.LoadScene("SlotMachine_"+HomeScript.isSlotNumber);
        yield return 0;
    }
}
