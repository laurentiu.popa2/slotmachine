﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class HomeScript : MonoBehaviour {

    public Animator LoadingAnimator;
    public static int isSlotNumber=0;
    private ListPositionCtrl3 _listPosition;

    void Start()
	{
        isSlotNumber = 0;
       if (LoadingAnimator.gameObject.activeSelf)
       {
           LoadingAnimator.Rebind();
       }
       
       Music.THIS.GetComponent<AudioSource> ().Stop ();
    }

    public void ClickBtn()
	{
		FXSound.THIS.fxSound.PlayOneShot (FXSound.THIS.ButtonClick);
	}

   public void SlotMachine1()
   {
        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.ButtonClick);
        isSlotNumber = 1;
        MusicController.Music.audiosource.enabled = false;
        if (LoadingAnimator.gameObject.activeSelf)
        {
            LoadingAnimator.SetTrigger("LoadingOut");
        }

        if(DataManager.Instance.CheckTutorial<1)
             DataManager.Instance.AddCheckFirsTime(1);
        Debug.Log("Clicked SlotMachine1");
    }
   


   public void ExitToHome()
    {
        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.ButtonClick);
        SceneManager.LoadScene("HomeScene");
    }
}
