﻿using UnityEngine;
public class AspectCamera : MonoBehaviour {

	// Use this for initialization
	void Start () {
		EnableCamera ();
	}
	
	void EnableCamera ()
	{
		//--
		float aspect = (float)Screen.width / (float)Screen.height;

		float aspects = (float)Mathf.Round (aspect * 100) / 100f;
		if (aspects >2.10f&& aspects<2.3f)    
			GetComponent<Camera> ().orthographicSize = 14.54f;
		else if (aspects>2.01&& aspects<2.09)      
			GetComponent<Camera> ().orthographicSize = 14.54f;    
		else if (aspects == 1.78f)  //16:9
			GetComponent<Camera> ().orthographicSize = 14.3f;          
		else if (aspects == 1.71f)
			GetComponent<Camera> ().orthographicSize = 14.97f;   
		else if (aspects == 1.67f)      //5:3
			GetComponent<Camera> ().orthographicSize = 14.67f;  
		else if (aspects == 1.6f)
			GetComponent<Camera> ().orthographicSize = 15.10f;        
		else if (aspects == 1.5f)    //3:2
			GetComponent<Camera> ().orthographicSize = 16.4f;
		else if (aspects == 1.33f)    //3:2
			GetComponent<Camera> ().orthographicSize = 18.01f;
		else
			GetComponent<Camera> ().orthographicSize =15.01f; ;
	}
}
