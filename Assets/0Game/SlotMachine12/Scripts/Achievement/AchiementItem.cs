﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AchiementItem : MonoBehaviour {
	
	public Text text_infor, text_Money, text_Name;
	public Button btn_Money;
	public string KeyPlayerpref;
	public int TargetProgress, Money,Id;
	public GameObject DoneAchi, Icon;
    public GameObject BorderAchi;
    public ParticleSystem CoinsPar;


	void Start () {
		btn_Money.onClick.AddListener (() => OnClickMoney ());
	}

	public void SetValueAchi(int id,string infor1, string infor2, string key, int progress, int money){
		KeyPlayerpref = key;
		TargetProgress = progress;
		text_Money.text = money.ToString("n0");
		Money = money;
		Id = id;
		text_Name.text = infor1;
		int value = PlayerPrefs.GetInt (key);
		if (value < progress) {
			btn_Money.gameObject.SetActive (false);
            BorderAchi.SetActive(false);
            DoneAchi.SetActive (false);
			text_infor.text = infor1+" <color=yellow>" +  value + "/" + progress + "</color> "+infor2;
		} else {
			if (PlayerPrefs.GetInt ("Achi" + id) == 0) {
				btn_Money.gameObject.SetActive (true);
                BorderAchi.SetActive(true);
                DoneAchi.SetActive (false);
			} else {
				btn_Money.gameObject.SetActive (false);
                BorderAchi.SetActive(false);
                DoneAchi.SetActive (true);
			}
			text_infor.text = infor1+" <color=yellow>" +  progress + "/" + progress + "</color> "+infor2;
		}
	}
	/// <summary>
	/// Playerpref: "Achi"+id = 0 -> can take money, =1 take done
	/// </summary>
	void OnClickMoney(){
        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.WinSmall);
        DataManager.Instance.AddCoins(Money);
		DoneAchi.SetActive (true);
        CoinsPar.Play();
        BorderAchi.SetActive(false);
        btn_Money.interactable = false;
        PlayerPrefs.SetInt ("Achi" + Id, 1);
        Invoke("DisActiveBtn",3.0f);

	}
    public void DisActiveBtn()
    {
        btn_Money.gameObject.SetActive(false);
    }

}
