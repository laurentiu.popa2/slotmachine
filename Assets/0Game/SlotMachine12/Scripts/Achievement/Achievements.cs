﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;


[System.Serializable]
public class Achievement {
	public string infor1, infor2, key;
	public int money, totalProgress;
}

public class Achievements : MonoBehaviour {
	public Button btn_Back;
    public GameObject panel_AchiScroll, AchievementItem;

    //public GameObject panel_Achievements,panel_Scroll,GameObject_parent;
	public Achievement[] achi;
	private int count = 0;
	private AchievemenManager _achievementManager;

	void Start () {
		btn_Back.onClick.AddListener (() => OnClickBack ());
        _achievementManager = GameObject.Find("AchievementObj").GetComponent<AchievemenManager>();

    }

	void OnEnable(){
		if (count == 0) {
			UpdateAchi ();
			count++;
		}

	}

	public void OnClickBack()
	{
        _achievementManager.LoadAchi ();
        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Click2);
	}
    IEnumerator ASetActiveFalse()
    {
        this.gameObject.SetActive(false);
        yield return 0; 
    }

	void UpdateAchi()
	{
		for (int i = 0; i < achi.Length; i++) {
			if (PlayerPrefs.GetInt ("Achi" + i) == 0) {
				int value = PlayerPrefs.GetInt (achi[i].key);
				if (value < achi [i].totalProgress) {
				} else
					Instantiate_itemAchi (i);
			}
		}
		for (int i = 0; i < achi.Length; i++) {
			if (PlayerPrefs.GetInt ("Achi" + i) == 0) {
				int value = PlayerPrefs.GetInt (achi[i].key);
				if (value < achi[i].totalProgress) {
					Instantiate_itemAchi (i);
				}
			}
		}
		for (int i = 0; i < achi.Length; i++) {
			if (PlayerPrefs.GetInt ("Achi" + i) == 1) {
				Instantiate_itemAchi (i);
			}
		}
	}
	public void Instantiate_itemAchi( int i )
	{
		GameObject newAchi = AchievementItem.Spawn ();
		AchiementItem ThisAchi = newAchi.GetComponent<AchiementItem> ();
		ThisAchi.SetValueAchi (i, achi [i].infor1, achi [i].infor2, achi [i].key, achi [i].totalProgress, achi [i].money);
		newAchi.transform.SetParent (panel_AchiScroll.transform);
		newAchi.transform.localScale = AchievementItem.transform.localScale;
        newAchi.transform.localPosition= new Vector3(AchievementItem.transform.localPosition.x, AchievementItem.transform.localPosition.y, 0);
    }
}
