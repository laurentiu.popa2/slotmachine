﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AchievemenManager : MonoBehaviour {


    public GameObject PanelBtn;
    public GameObject RedCircle;
    public GameObject panel_Achi;
    public Text Text_Achi;
    public Animator AchievementAni;
    private ListPositionCtrl3 _listPosition;
    // Use These PlayerPref "keyname"
    // For save and confirm achievements completed
    //--------------------------------------------
    // Spin Time: "SpinTimes"
    // Hit Scatter: "ScatterGames"
    // Hit Bonus Game: "BonusGames"
    // OPEN MISTERY BOX: "MisteryBox"
    // WATCH VIDEO: "WatchVideo"
    //--------------------------------------------
    //Exp: PlayerPrefs.SetInt ("ScatterGames", PlayerPrefs.GetInt("ScatterGames") +1);
    //--------------------------------------------
    void Start()
    {
        LoadAchi();
        AchievementAni.Rebind();
        _listPosition = GameObject.Find("PanelBtn").GetComponent<ListPositionCtrl3>();
        _listPosition.enabled = true;
    }
    public void LoadAchi()
    {
        StartCoroutine(LoadDataGame());
    }
    public void ShowAchievement()
    {

        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.AchievementPopup);
        panel_Achi.SetActive(true);
        _listPosition.enabled = false;
        if (AchievementAni.gameObject.activeSelf)
            AchievementAni.SetTrigger("AchiOn");

    }
    public void HideAchievement()
    {
        //LoadDataGame();
        if (AchievementAni.gameObject.activeSelf)
            AchievementAni.SetTrigger("AchiOff");
        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Click2);
        _listPosition.enabled = true;
    }
    IEnumerator LoadDataGame()
    {
        yield return new WaitForSeconds(1f);
        int count = 0;
        Achievements Achi = panel_Achi.GetComponent<Achievements>();
        for (int i = 0; i < Achi.achi.Length; i++)
        {
            if (PlayerPrefs.GetInt("Achi" + i) == 0)
            {
                int value = PlayerPrefs.GetInt(Achi.achi[i].key);
                if (value < Achi.achi[i].totalProgress)
                {
                }
                else
                    count++;
            }
        }
        if (count > 0)
        {
            RedCircle.SetActive(true);
            Text_Achi.text = "" + count;
        }
        else
        {
            RedCircle.SetActive(false);
            Text_Achi.text = "";
        }
    }
}
