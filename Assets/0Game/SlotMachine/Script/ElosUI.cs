﻿using System;
using SLFramework;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Elona.Slot {
	public class ElosUI : BaseSlotGameUI {
		[Serializable]
		public class Colors {
			public Gradient freeSpinBG;
			public Gradient freeSpinBGSlot;
		}

		[Header("Elos")] public Elos elos;
    	//public ElosShop shop;
		public Colors colors;

		public Image background, highlightFreeSpin, backgroundSlot;
		public Button buttonPlay;
//		public Slider sliderExp;
		public GameObject payTable;
		public GameObject[] BGs;
		private int indexBG;

		private Tweener _moneyTween;
		private int lastBalance;
       

		private Elos.Assets assets { get { return elos.assets; } }
		private void OnEnable() {
		}

		public override void Initialize() {
			base.Initialize();
			slot.callbacks.onAddBalance.AddListener(OnAddBalance);
			lastBalance = slot.gameInfo.balance;
			elos.bonusGame.gameObject.SetActive(false);
      
        }

        public override void OnActivated() {
			base.OnActivated();
			if (!slot.debug.skipIntro) {
                 FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Intro3);
                assets.tweens.tsIntro1.Play ();
              
			}
            Invoke("PlayMusic", 0.1f);
        }
        public void PlayMusic()
        {
            Music.THIS.GetComponent<AudioSource>().Stop();
            Music.THIS.GetComponent<AudioSource>().loop = true;
            Music.THIS.GetComponent<AudioSource>().clip = Music.THIS.music[2];
            Music.THIS.GetComponent<AudioSource>().Play();
        }


        public override void OnRoundStart() {
			base.OnRoundStart();
			if (slot.currentMode != slot.modes.freeSpinMode) buttonPlay.interactable = true;
		}

		public override void OnReelStart(SLFramework.ReelInfo info) {
            PlayerPrefs.SetInt("SpinTimes", PlayerPrefs.GetInt("SpinTimes") + 1);
            base.OnReelStart(info);
			if (info.isFirstReel) {
                FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Spin3);
                FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.SpinLoop3);
               
            }

		}

		public override void OnReelStop(SLFramework.ReelInfo info) {
			base.OnReelStop(info);
            FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.ReelStop3);
            if (info.isFirstReel) buttonPlay.interactable = false;
			if (info.isLastReel) {
                FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Spin3);
            }
		}

		public override void OnRoundComplete() 
		{
			if (slot.gameInfo.roundHits == 0) {
                FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Lose3);
            }
		}

		public override void EnableNextLine() {
			if (!slot.lineManager.EnableNextLine ())

                  FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Beep3);
            else
                FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Bet3);

        }

		public override void DisableCurrentLine() {
			if (!slot.lineManager.DisableCurrentLine ())
                FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Beep3);

            else {
                FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Bet3);

            }
		}

		public override bool SetBet(int index) {
			if (!base.SetBet(index)) {
                FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Beep3);
                return false;
			}
            FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Bet3);
            return true;
		}

		public void TogglePayTable() {
            FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.ButtonClick);
            payTable.SetActive(!payTable.activeSelf);
		}

		public void ToggleShop() {
		}

		public override void ToggleFreeSpin(bool enable) {
			base.ToggleFreeSpin(enable);
			if (enable) {
				assets.particleFreeSpin.Play();
				backgroundSlot.DOGradientColor(colors.freeSpinBGSlot, 0.6f);
				background.DOGradientColor(colors.freeSpinBG, 0.6f);
			} else {
				assets.particleFreeSpin.Stop();
				backgroundSlot.DOColor(Color.white, 2f);
				background.DOColor(Color.white, 2f);
			}
		}

		public override void OnProcessHit(SLFramework.HitInfo info) {
			base.OnProcessHit(info);
			SymbolHolder randomHolder = info.hitHolders[Random.Range(0, info.hitHolders.Count)];
			ElosSymbol symbol = randomHolder.symbol as ElosSymbol;
			Util.InstantiateAt<ElosEffectBalloon>(assets.effectBalloon, slot.transform.parent, randomHolder.transform).Play(symbol.GetRandomTalk());
			foreach (SymbolHolder holder in info.hitHolders) info.sequence.Join(ShowWinAnimation(info, holder));
		}

		// Winning particle and audio effect when a line is a "hit"
		public Tweener ShowWinAnimation(SLFramework.HitInfo info, SymbolHolder holder) {
			return Util.Tween(() => {
				int coins = (info.hitChains - 2)*(info.hitChains - 2)*(info.hitChains - 2) + 1;

				if (info.hitSymbol.payType == Symbol.PayType.Normal) {
					assets.particlePrize.transform.position = holder.transform.position;
					Util.Emit(assets.particlePrize, coins);
					if (info.hitChains <= 3)
					{
                        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.WinSmall);
                    }
					else if (info.hitChains == 4) 
					{
                        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.WinMedium);
                    }
					else {
                        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.WinSpecial);
                    }
					if (info.hitChains >= 4) assets.tweens.tsWin.SetText(info.hitChains + "-IN-A-ROW!", info.hitChains*40).Play();
				} else {
                    FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.WinSpecial);
                    if (info.hitSymbol.payType == Symbol.PayType.FreesSpin)
                    {
                        assets.tweens.tsWinSpecial.SetText("Free Spin!").Play();
                        PlayerPrefs.SetInt("ScatterGames", PlayerPrefs.GetInt("ScatterGames") + 1);
                    }
                    else
                    {
                        assets.tweens.tsWinSpecial.SetText("BONUS!").Play();
                        PlayerPrefs.SetInt("BonusGames", PlayerPrefs.GetInt("BonusGames") + 1);
                    }
                       

				}
			});
		}

		private int _lastBalance;

		private void Update() {
			textMoney.text = DataManager.Instance.Coins.ToString("n0"); 
			if (_lastBalance != lastBalance) {
				_lastBalance = lastBalance;
			}
		}

		public override void RefreshMoney() { }

		public void OnAddBalance(BalanceInfo info) {
			if (info.amount == 0) return;

			float duration = 1f;
			if (info.amount < 0) {
                FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.Pay3);
                Util.Emit(assets.particlePay, 3);
			} else {
                PlayerPrefs.SetInt("LastWinll",  info.amount);
                PlayerPrefs.Save();
                if (info.hitInfo != null) {
                    if (info.hitInfo.hitChains <= 3)
                        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.EarnSmall);
                    else FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.EarnBig);
                    duration = slot.effects.GetHitEffect(info.hitInfo).duration*0.8f;
				} else {
                    FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.EarnSmall);
                }
			}

			Util.InstantiateAt<ElosEffectMoney>(assets.effectMoney, transform).SetText(info.amount, info.hitInfo == null ? "" : info.hitInfo.hitChains + " IN A ROW!").Play(100, 2.0f);
			if (_moneyTween != null && _moneyTween.IsPlaying()) _moneyTween.Complete();
			_moneyTween = DOTween.To(() => lastBalance, x => lastBalance = x, slot.gameInfo.balance, duration).OnComplete(() => { _moneyTween = null; });
		}
	}
}