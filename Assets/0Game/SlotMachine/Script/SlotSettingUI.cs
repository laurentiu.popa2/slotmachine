﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;
public class SlotSettingUI : MonoBehaviour 
{
    //public Button VideoAdsBtn;
    //public GameObject finger,overlayMoney;
    public Animator LoadingAnimator;

    public GameObject overlay;
	public GameObject OptionsObj;
	public GameObject ExitGameObj;
	public Transform target;
	public EaseType easeTypeGoTo = EaseType.EaseInOutBack;

	bool isOpen = false;
	Vector3 oriPos = Vector3.zero;
	//public GameObject SettingBtn;
	public GameObject QuitBtn;
	//public GameObject FreeCoins;
	public static int IsFreeAds=0;
	void Awake()
	{
		//SettingBtn.SetActive (false);
		QuitBtn.SetActive (true);
		//FreeCoins.SetActive (false);
		//Invoke ("SoundBtnFunction",2.5f);
	}
	public void SoundBtnFunction()
	{
		//SettingBtn.SetActive (true);
		QuitBtn.SetActive (true);
		//FreeCoins.SetActive (true);
	}
	public void LoadHomeScene()
	{
        //ads
        //AdmobBannerController.Instance.ShowInterstitial();
        //AdManagerUnity.Instance.ShowAd("video");
        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.ButtonClick);
        if (LoadingAnimator.gameObject.activeSelf)
        {
            //LoadingAnimator.Rebind();
            LoadingAnimator.SetTrigger("LoadingOut");
        }
	}
	public void Move(Transform tr) 
	{
        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.ButtonClick);

        if (!isOpen)
		{
			if (tr.name == "Quit Game") 
			{

			}
			overlay.SetActive(true);
			oriPos = tr.position;
			TweenParms parms = new TweenParms().Prop("position", target.position).Ease(easeTypeGoTo);
			HOTween.To(tr, .7f, parms);
			isOpen = true;
			ExitGameObj.SetActive (true);
			OptionsObj.SetActive (true);
		}
		else 
		{
			overlay.SetActive(false);
			TweenParms parms = new TweenParms().Prop("position", oriPos).Ease(easeTypeGoTo).OnComplete(OnComplete);
			HOTween.To(tr, .7f, parms);

			ExitGameObj.SetActive (false);
			OptionsObj.SetActive (false);
		}

	}
	void OnComplete() 
	{
		isOpen = false;
	}

	public void BackToHomeScene()
	{
        //ads
        // AllServicesControls.Instance.ShowInterstitials();
        FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.ButtonClick);
        if (LoadingAnimator.gameObject.activeSelf)
        {
            LoadingAnimator.SetTrigger("LoadingOut");
        }
    }
	public void ShowUnityAds()
	{
        if (DataManager.Instance.Coins < 5)
        {
            IsFreeAds = 1;
            FXSound.THIS.fxSound.PlayOneShot(FXSound.THIS.ButtonClick);
            // AllServicesControls.Instance.ShowRewardedAds();

        }
	}
	public void ReshowStartBtn()
	{
		SceneManager.LoadScene ("SlotMachine_3");
	}


}
