﻿using UnityEngine;
using System;
using System.Collections;



public class DataManager : MonoBehaviour 
{

	public static DataManager Instance;


	public int CheckTutorial
	{ 
		get { return _checkfirsttime; }
		private set { _checkfirsttime = value; }
	}
	public int Coins
	{ 
		get { return _initCoin; }
		set { _initCoin = value; }
	}
	public int FluxCoins
	{ 
		get { return _fluxCoin; }
		private set { _fluxCoin = value; }
	}
    public int FreeAdNumber
    {
        get { return _freeadsnumber; }
        private set { _freeadsnumber = value; }
    }



    public static event Action<int> CheckTutorialUpdated = delegate {};
	public static event Action<int> CoinsUpdated = delegate {};
	public static event Action<int> CoinsFluxUpdated = delegate {};
    public static event Action<int> FreeAdsNumberUpdated = delegate {};
    [SerializeField]
	int initialCheckTutorial = 0;
	[SerializeField]
	int _checkfirsttime;

	[SerializeField]
	int initialCoins = 10000000;
	[SerializeField]
	int _initCoin;


	[SerializeField]
	int _fluxCoin;
    [SerializeField]
    int initialFreeADS = 10;
    [SerializeField]
    int _freeadsnumber;

	void Start()
	{
		ResetCheckFirsTime ();
		ResetCoins ();
		ResetFluxCoins ();
        // ResetFreeAds();
    
    }
    void Awake()
	{
        //PlayerPrefs.DeleteAll ();
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        if (CheckTutorial < 1)
        {
            PlayerPrefs.SetInt("Music", 1);
            PlayerPrefs.SetInt("Sound", 1);
        }
    }

	public void ResetCheckFirsTime()
	{
		CheckTutorial = PlayerPrefs.GetInt("FirstTimePlay", initialCheckTutorial);
	}

	public void AddCheckFirsTime(int amount)
	{
		CheckTutorial += amount;
		PlayerPrefs.SetInt("FirstTimePlay", CheckTutorial);
		CheckTutorialUpdated(CheckTutorial);
	}

	public void RemoveCheckFirsTime(int amount)
	{
		CheckTutorial -= amount;
		PlayerPrefs.SetInt("FirstTimePlay", CheckTutorial);
		CheckTutorialUpdated(CheckTutorial);
	}
    //----------------------
    // public void ResetFreeAds()
    // {
    //     FreeAdNumber = PlayerPrefs.GetInt("FreeAdNumber", initialFreeADS);
    // }

    // public void AddFreeAdNumber(int amount)
    // {
    //     FreeAdNumber += amount;
    //     PlayerPrefs.SetInt("FreeAdNumber", FreeAdNumber);
    //     FreeAdsNumberUpdated(FreeAdNumber);
    // }

    public void RemoveFreeAdNumber(int amount)
    {
        FreeAdNumber -= amount;
        PlayerPrefs.SetInt("FreeAdNumber", FreeAdNumber);
        FreeAdsNumberUpdated(FreeAdNumber);
    }

    //----------------------
    public void ResetCoins()
	{
		Coins = PlayerPrefs.GetInt("CoinsGame", initialCoins);
	}

	public void AddCoins(int amount)
	{
		Coins += amount;
		PlayerPrefs.SetInt("CoinsGame", Coins);
		CoinsUpdated(Coins);
	}

	public void RemoveCoins(int amount)
	{
		Coins -= amount;
		PlayerPrefs.SetInt("CoinsGame", Coins);
		CoinsUpdated(Coins);
	}
	//--------
	public void ResetFluxCoins()
	{
		PlayerPrefs.SetInt("CoinsFluxGame", PlayerPrefs.GetInt("CoinsGame"));
		_fluxCoin = Coins;
	}
	public void AddFluxCoins(int amount)
	{
		_fluxCoin += amount;
		PlayerPrefs.SetInt("CoinsFluxGame", _fluxCoin);
		CoinsFluxUpdated(FluxCoins);
	}

}
