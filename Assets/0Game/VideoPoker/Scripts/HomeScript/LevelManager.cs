﻿using System.Collections;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
	public static LevelManager THIS;
	Animator animconvert;
	public GameObject bgOverlay;
	void Start () 
	{
		THIS = this;
		animconvert = GetComponentInChildren<Animator> ();
		StartCoroutine (InLoadScene ());

	}

	//-------------AUTO LOAD SCENE WHEN FIRST SCENCE------------------
	public IEnumerator InLoadScene()
	{
		animconvert.SetTrigger ("First");
		bgOverlay.SetActive(true);
		SoundController.Sound.Out ();
		yield return new WaitForSeconds (1f);
		animconvert.SetTrigger ("Out");
		SoundController.Sound.In ();
		MusicController.Music.BG_Home ();
		bgOverlay.SetActive(false);
	}
}
