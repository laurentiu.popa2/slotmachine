﻿using UnityEngine;
using UnityEngine.UI;
using Holoville.HOTween;
public class HomePopup : MonoBehaviour
{
	public Text CoinTex;
	public GameObject OptionsObj;
	public GameObject ExitGameObj;
	public EaseType easeTypeGoTo = EaseType.EaseInOutBack;
	public Transform target;
	public GameObject overlay;
	public GameObject SpinObj;

	Vector3 oriPos = Vector3.zero;
	bool isOpen = false;
	void Start()
	{
		SpinObj.SetActive (false);
	}
	
	void Update()
	{
		CoinTex.text = "" + DataManager.Instance.Coins;
	}
	
	public void Move(Transform tr) 
	{
		if (!isOpen)
		{
			if (tr.name == "Quit Game") 
			{
				ExitGameObj.SetActive (true);
				OptionsObj.SetActive (false);
			}
			if (tr.name == "Setting") 
			{
				ExitGameObj.SetActive (false);
				OptionsObj.SetActive (true);
			}
			overlay.SetActive(true);
			oriPos = tr.position;
			TweenParms parms = new TweenParms().Prop("position", target.position).Ease(easeTypeGoTo);
			HOTween.To(tr, .7f, parms);
			isOpen = true;
			SoundController.Sound.PopupShow ();
		}
		else 
		{
			SoundController.Sound.CloseBtn ();
			overlay.SetActive(false);
			TweenParms parms = new TweenParms().Prop("position", oriPos).Ease(easeTypeGoTo).OnComplete(OnComplete);
			HOTween.To(tr, .7f, parms);
			ExitGameObj.SetActive (false);
			OptionsObj.SetActive (false);
		}
	}

	void OnComplete()
	{
		isOpen = false;
	}
	
	public void SpinScene()
	{
		SpinObj.SetActive (true);
		SoundController.Sound.PopupShow ();	
	}
	
	public void CloseLuckyWheel()
	{
		SoundController.Sound.CloseBtn ();
	}
	
}
