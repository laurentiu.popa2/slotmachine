﻿using UnityEngine;

/// <summary>
/// sound in game : button, effect, win, lose...
/// </summary>
public class SoundController : MonoBehaviour
{

    public static SoundController Sound; // instance of SoundController

    public AudioClip[] SoundClips;      // array sound clips

    public AudioSource audiosource;     // audio source
    void Awake()
    {
        if (Sound == null)
        {
            DontDestroyOnLoad(gameObject);
            Sound = this;
        }
        else if (Sound != this)
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// sound on state
    /// </summary>
    public void SoundON()
    {
        audiosource.mute = false;
    }

    /// <summary>
    /// sound off state
    /// </summary>
    public void SoundOFF()
    {
        audiosource.mute = true;
    }

    public void ClickBtn()
    {
        audiosource.PlayOneShot(SoundClips[0]);

    }
    public void VideoPoker_Lose()
	{
		audiosource.PlayOneShot(SoundClips[2]);
	}
	
	public void PopupShow()
    {
        audiosource.PlayOneShot(SoundClips[4]);
    }

	public void VideoPoker_Win()
    {
        audiosource.PlayOneShot(SoundClips[5]);
    }
    public void BlackJack_Start()
    {
        audiosource.PlayOneShot(SoundClips[6]);
    }
	public void OpenGift()
	{
		audiosource.PlayOneShot(SoundClips[7]);
	}
	public void CloseBtn()
	{
		audiosource.PlayOneShot(SoundClips[8]);
	}
	public void BlackJack_Win()
	{
		audiosource.PlayOneShot(SoundClips[9]);
	}
	public void DisactiveButtonSound()
	{
		audiosource.PlayOneShot(SoundClips[10]);
	}
	public void OpenGiftEnd()
	{
		audiosource.PlayOneShot(SoundClips[11]);
	}
	public void CallBackSuccess()
	{
		audiosource.PlayOneShot(SoundClips[12]);
	}
	public void SpinningSound()
	{
		audiosource.PlayOneShot(SoundClips[13]);
	}
	public void In()
	{
		audiosource.PlayOneShot(SoundClips[14]);
	}
	public void Out()
	{
		audiosource.PlayOneShot(SoundClips[15]);
	}
}
