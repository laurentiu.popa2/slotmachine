using UnityEngine;
using UnityEngine.SceneManagement;

public class HomeCustomisationPopup : MonoBehaviour
{
    [SerializeField] private GameObject overlay;
    [SerializeField] private Animator loadingAnimator;
    [SerializeField] private GameObject customisationPopup;

    
    public void OpenCustomisationPopup()
    {
        SoundController.Sound.PopupShow ();
        overlay.SetActive(true);
        customisationPopup.SetActive(true);
    }
    
    public void CloseCustomisationPopup()
    {
        SoundController.Sound.CloseBtn ();
        overlay.SetActive(false);
        customisationPopup.SetActive(false);
    }
    
    public void GirlCustomisationBtn()
    {
        MusicController.Music.audiosource.enabled = true;
        if (loadingAnimator.gameObject.activeSelf)
        {
            loadingAnimator.SetTrigger("LoadingOut");
        }
        
        PlayerPrefs.SetInt("IsGirl", 1);
        PlayerPrefs.SetInt("IsBoy", 0);
        PlayerPrefs.Save();
        SceneManager.LoadScene("GirlCustomisationScene");
    }
    
    public void BoyCustomisationBtn()
    {
        MusicController.Music.audiosource.enabled = true;
        if (loadingAnimator.gameObject.activeSelf)
        {
            loadingAnimator.SetTrigger("LoadingOut");
        }
        
        PlayerPrefs.SetInt("IsGirl", 0);
        PlayerPrefs.SetInt("IsBoy", 1);
        PlayerPrefs.Save();
        SceneManager.LoadScene("BoyCustomisationScene");
    }
}
