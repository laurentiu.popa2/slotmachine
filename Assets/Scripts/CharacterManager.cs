using UnityEngine;

public class CharacterManager : MonoBehaviour
{
    [Header("Boy")]
    [SerializeField] private GameObject boy;
    [SerializeField] private GameObject[] boyHair;
    [SerializeField] private GameObject[] boyOutfit;
    
    [Header("Girl")]
    [SerializeField] private GameObject girl;
    [SerializeField] private GameObject[] girlHair;
    [SerializeField] private GameObject[] girlOutfit;
    private void Start()
    {
        if (PlayerPrefs.GetInt("IsBoy") == 1)
        {
            foreach (var h in boyHair)
            {
                h.SetActive(false);
            }

            foreach (var fit in boyOutfit)
            {
                fit.SetActive(false);
            }
            
            boyHair[PlayerPrefs.GetInt("BoyHair")].SetActive(true);
            boyOutfit[PlayerPrefs.GetInt("BoyOutfit")].SetActive(true);
            boy.SetActive(true);
        }
        
        if (PlayerPrefs.GetInt("IsGirl") == 1)
        {
            foreach (var h in boyHair)
            {
                h.SetActive(false);
            }

            foreach (var fit in boyOutfit)
            {
                fit.SetActive(false);
            }
            
            girlHair[PlayerPrefs.GetInt("hair")].SetActive(true);
            girlOutfit[PlayerPrefs.GetInt("outfit")].SetActive(true);
            girl.SetActive(true);
        }
    }

}
