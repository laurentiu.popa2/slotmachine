using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BoyCustomisationController : MonoBehaviour
{
    private int _hairID;
    private int _outfitID;

    private int _firstHairIsBought;
    private int _secondHairIsBought;
    private int _thirdHairIsBought;
    private int _fourthHairIsBought;
    
    private int _firstOutfitIsBought;
    private int _secondOutfitIsBought;
    private int _thirdOutfitIsBought;
    private int _fourthOutfitIsBought;

    [Header("Customisation")]
    [SerializeField] private GameObject[] hair;
    [SerializeField] private GameObject[] outfit;

    [Header("Hair Price")] 
    [SerializeField] private int[] haiPrice;
    [SerializeField] private Text[] textHairPrice;
    [SerializeField] private Button[] buttonsHairPrice;
    [SerializeField] private Button[] buttonsHairEquip;
    
    [Header("Outfit Price")] 
    [SerializeField] private int[] outfitPrice;
    [SerializeField] private Text[] textOutfitPrice;
    [SerializeField] private Button[] buttonsOutfitPrice;
    [SerializeField] private Button[] buttonsOutfitEquip;

    [Header("Cameras")] 
    [SerializeField] private GameObject[] cams;

    [Header("UI Elements")] 
    [SerializeField] private GameObject btnPanel;
    [SerializeField] private GameObject[] itemsPanel;
    [SerializeField] private GameObject backToItemBtn;
    [SerializeField] private GameObject backToStartSceneBtn;
    [SerializeField] private GameObject hairBtn;
    [SerializeField] private GameObject outfitBtn;

    [Header("Initial Characters")] 
    [SerializeField] private GameObject initialChar1;
    [SerializeField] private GameObject initialChar2;
    [SerializeField] private GameObject initialChar3;

    public Animator LoadingAnimator;


    private void Awake()
    {
        _hairID = PlayerPrefs.GetInt("BoyHair", 0);
        _outfitID = PlayerPrefs.GetInt("BoyOutfit", 0);
    }

    private void Start()
    {
        SetItem("hair");
        SetItem("outfit");
        
        SetTextPrices();

        hairBtn.GetComponent<Button>().onClick.AddListener(HairBtn);
        outfitBtn.GetComponent<Button>().onClick.AddListener(OutfitBtn);
        backToItemBtn.GetComponent<Button>().onClick.AddListener(BackToItemsBtn);
        backToStartSceneBtn.GetComponent<Button>().onClick.AddListener(BackToStartScene);
    }

    private void SetTextPrices()
    {
        //Set Hair Initial Price
        textHairPrice[0].text = haiPrice[0].ToString();
        textHairPrice[1].text = haiPrice[1].ToString();
        textHairPrice[2].text = haiPrice[2].ToString();
        // textHairPrice[3].text = haiPrice[3].ToString();
        
        //Set Outfit Initial Price
        textOutfitPrice[0].text = outfitPrice[0].ToString();
        textOutfitPrice[1].text = outfitPrice[1].ToString();
        textOutfitPrice[2].text = outfitPrice[2].ToString();
        // textOutfitPrice[3].text = outfitPrice[3].ToString();
    }

    public void SelectHair(bool isForward)
    {
        if (isForward)
        {
            if (_hairID == hair.Length - 1)
                _hairID = 0;
            else
                _hairID++;
        }
        else
        {
            if (_hairID == 0)
                _hairID = hair.Length - 1;
            else
                _hairID--;
        }
        

        SetItem("hair");
    }

    public void SelectOutfit(bool isForward)
    {
        if (isForward)
        {
            if (_outfitID == outfit.Length - 1)
                _outfitID = 0;
            else
                _outfitID++;
        }
        else
        {
            if (_outfitID == 0)
                _outfitID = outfit.Length - 1;
            else
                _outfitID--;
        }
        
        SetItem("outfit");
    }

    private void HairBtn()
    {
        foreach (var cam in cams)
        {
            cam.SetActive(false);
        }
        foreach (var item in itemsPanel)
        {
            item.SetActive(false);
        }

        cams[1].SetActive(true);
        btnPanel.SetActive(false);
        backToStartSceneBtn.SetActive(false);
        backToItemBtn.SetActive(true);
        itemsPanel[0].SetActive(true);
    }

    private void OutfitBtn()
    {
        foreach (var cam in cams)
        {
            cam.SetActive(false);
        }
        foreach (var item in itemsPanel)
        {
            item.SetActive(false);
        }

        cams[0].SetActive(true);
        btnPanel.SetActive(false);
        backToStartSceneBtn.SetActive(false);
        backToItemBtn.SetActive(true);
        itemsPanel[1].SetActive(true);
    }

    private void BackToItemsBtn()
    {
        foreach (var cam in cams)
        {
            cam.SetActive(false);
        }
        foreach (var panel in itemsPanel)
        {
            panel.SetActive(false);
        }
        foreach (var h in hair)
        {
            h.SetActive(false); 
        }
        foreach (var fit in outfit)
        {
            fit.SetActive(false);
            initialChar1.SetActive(false);
            initialChar2.SetActive(false);
            initialChar3.SetActive(false);
        }
        
        hair[PlayerPrefs.GetInt("BoyHair")].SetActive(true);
        outfit[PlayerPrefs.GetInt("BoyOutfit")].SetActive(true);
        initialChar1.SetActive(true);
        initialChar2.SetActive(true);
        initialChar3.SetActive(true);

        cams[0].SetActive(true);
        btnPanel.SetActive(true);
        backToStartSceneBtn.SetActive(true);
        backToItemBtn.SetActive(false);
        SoundController.Sound.CloseBtn ();
    }

    private void BackToStartScene()
    {
        MusicController.Music.audiosource.enabled = true;
        SoundController.Sound.CloseBtn ();
        if (LoadingAnimator.gameObject.activeSelf)
        {
            LoadingAnimator.SetTrigger("LoadingOut");
        }

        SceneManager.LoadScene("HomeScene");
    }

    private void HairEquipButtonsChecker()
    {
        if (PlayerPrefs.GetInt("firstHairIsBought") == 1 && _hairID == 0)
            buttonsHairEquip[0].gameObject.SetActive(true);
        else if (PlayerPrefs.GetInt("secondHairIsBought") == 1 && _hairID == 1)
            buttonsHairEquip[1].gameObject.SetActive(true);
        else if (PlayerPrefs.GetInt("thirdHairIsBought") == 1 && _hairID == 2)
            buttonsHairEquip[2].gameObject.SetActive(true);
        else if (PlayerPrefs.GetInt("fourthHairIsBought") == 1 && _hairID == 3)
            buttonsHairEquip[3].gameObject.SetActive(true);
        else
            buttonsHairPrice[_hairID].gameObject.SetActive(true);
    }
    
    private void OutfitEquipButtonsChecker()
    {
        if (PlayerPrefs.GetInt("firstOutfitIsBought") == 1 && _outfitID == 0)
            buttonsOutfitEquip[0].gameObject.SetActive(true);
        else if (PlayerPrefs.GetInt("secondOutfitIsBought") == 1 && _outfitID == 1)
            buttonsOutfitEquip[1].gameObject.SetActive(true);
        else if (PlayerPrefs.GetInt("thirdOutfitIsBought") == 1 && _outfitID == 2)
            buttonsOutfitEquip[2].gameObject.SetActive(true);
        else if (PlayerPrefs.GetInt("fourthOutfitIsBought") == 1 && _outfitID == 3)
            buttonsOutfitEquip[3].gameObject.SetActive(true);
        else
            buttonsOutfitPrice[_outfitID].gameObject.SetActive(true);
    }

    private void SetItem(string type)
    {
        switch (type)
        {
            case "hair":
                foreach (var h in hair)
                {
                    h.SetActive(false); 
                }

                foreach (var b in buttonsHairPrice)
                {
                    b.gameObject.SetActive(false);
                }

                foreach (var b in buttonsHairEquip)
                {
                    b.gameObject.SetActive(false);
                }
                
                HairEquipButtonsChecker();
                hair[_hairID].SetActive(true);
                break;
            case "outfit":
                foreach (var fit in outfit)
                {
                    fit.SetActive(false);
                    initialChar1.SetActive(false);
                    initialChar2.SetActive(false);
                    initialChar3.SetActive(false);
                }
                
                foreach (var b in buttonsOutfitPrice)
                {
                    b.gameObject.SetActive(false);
                }
                foreach (var b in buttonsOutfitEquip)
                {
                    b.gameObject.SetActive(false);
                }
                initialChar1.SetActive(true);
                initialChar2.SetActive(true);
                initialChar3 .SetActive(true);
                OutfitEquipButtonsChecker();
                outfit[_outfitID].SetActive(true);
                break;
        }
    }

    public void SetPriceForHair(int modelNum)
    {
        switch (modelNum)
        {
            case 0:
                if (haiPrice[0] < DataManager.Instance.Coins)
                {
                    DataManager.Instance.Coins -= haiPrice[0];
                    buttonsHairPrice[0].gameObject.SetActive(false);
                    buttonsHairEquip[0].gameObject.SetActive(true);
                    _firstHairIsBought = 1;
                    PlayerPrefs.SetInt("CoinsGame", DataManager.Instance.Coins);
                    PlayerPrefs.SetInt("firstHairIsBought", _firstHairIsBought);
                    PlayerPrefs.Save();
                    Debug.Log(_hairID);
                }
                break;
            case 1:
                if (haiPrice[1] < DataManager.Instance.Coins)
                {
                    DataManager.Instance.Coins -= haiPrice[1];
                    _secondHairIsBought = 1;
                    buttonsHairPrice[1].gameObject.SetActive(false);
                    buttonsHairEquip[1].gameObject.SetActive(true);
                    PlayerPrefs.SetInt("CoinsGame", DataManager.Instance.Coins);
                    PlayerPrefs.SetInt("secondHairIsBought", _secondHairIsBought);
                    PlayerPrefs.Save();
                }
                break;
            case 2:
                if (haiPrice[2] < DataManager.Instance.Coins)
                {
                    DataManager.Instance.Coins -= haiPrice[2];
                    _thirdHairIsBought = 1;
                    buttonsHairPrice[2].gameObject.SetActive(false);
                    buttonsHairEquip[2].gameObject.SetActive(true);
                    PlayerPrefs.SetInt("CoinsGame", DataManager.Instance.Coins);
                    PlayerPrefs.SetInt("thirdHairIsBought", _thirdHairIsBought);
                    PlayerPrefs.Save();
                }
                break;
            case 3:
                if (haiPrice[3] < DataManager.Instance.Coins)
                {
                    DataManager.Instance.Coins -= haiPrice[3];
                    _fourthHairIsBought = 1;
                    buttonsHairPrice[3].gameObject.SetActive(false);
                    buttonsHairEquip[3].gameObject.SetActive(true);
                    PlayerPrefs.SetInt("CoinsGame", DataManager.Instance.Coins);
                    PlayerPrefs.SetInt("fourthHairIsBought", _fourthHairIsBought);
                    PlayerPrefs.Save();
                }
                break;
        }
    }
    
    
    public void SetPriceForOutfit(int modelNum)
    {
        switch (modelNum)
        {
            case 0:
                if (outfitPrice[0] < DataManager.Instance.Coins)
                {
                    DataManager.Instance.Coins -= haiPrice[0];
                    buttonsOutfitPrice[0].gameObject.SetActive(false);
                    buttonsOutfitEquip[0].gameObject.SetActive(true);
                    _firstOutfitIsBought = 1;
                    PlayerPrefs.SetInt("CoinsGame", DataManager.Instance.Coins);
                    PlayerPrefs.SetInt("firstOutfitIsBought", _firstOutfitIsBought);
                    PlayerPrefs.Save();
                    Debug.Log(_hairID);
                }
                break;
            case 1:
                if (outfitPrice[1] < DataManager.Instance.Coins)
                {
                    DataManager.Instance.Coins -= haiPrice[1];
                    _secondOutfitIsBought = 1;
                    buttonsOutfitPrice[1].gameObject.SetActive(false);
                    buttonsOutfitEquip[1].gameObject.SetActive(true);
                    PlayerPrefs.SetInt("CoinsGame", DataManager.Instance.Coins);
                    PlayerPrefs.SetInt("secondOutfitIsBought", _secondOutfitIsBought);
                    PlayerPrefs.Save();
                }
                break;
            case 2:
                if (outfitPrice[2] < DataManager.Instance.Coins)
                {
                    DataManager.Instance.Coins -= haiPrice[2];
                    _thirdOutfitIsBought = 1;
                    buttonsOutfitPrice[2].gameObject.SetActive(false);
                    buttonsOutfitEquip[2].gameObject.SetActive(true);
                    PlayerPrefs.SetInt("CoinsGame", DataManager.Instance.Coins);
                    PlayerPrefs.SetInt("thirdOutfitIsBought", _thirdOutfitIsBought);
                    PlayerPrefs.Save();
                }
                break;
            case 3:
                if (outfitPrice[3] < DataManager.Instance.Coins)
                {
                    DataManager.Instance.Coins -= haiPrice[3];
                    _fourthOutfitIsBought = 1;
                    buttonsOutfitPrice[3].gameObject.SetActive(false);
                    buttonsOutfitEquip[3].gameObject.SetActive(true);
                    PlayerPrefs.SetInt("CoinsGame", DataManager.Instance.Coins);
                    PlayerPrefs.SetInt("fourthOutfitIsBought", _fourthOutfitIsBought);
                    PlayerPrefs.Save();
                }
                break;
        }
    }

    public void EquipHair()
    {
        PlayerPrefs.SetInt("BoyHair", _hairID);
        PlayerPrefs.Save();
    }
    
    public void EquipOutfit()
    {
        PlayerPrefs.SetInt("BoyOutfit", _outfitID);
        PlayerPrefs.Save();
    }
}
