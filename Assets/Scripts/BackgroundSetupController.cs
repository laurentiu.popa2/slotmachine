using UnityEngine;

public class BackgroundSetupController : MonoBehaviour
{
    private int _bgID;
    [SerializeField] private GameObject[] backgrounds;
    [SerializeField] private GameObject[] disableOjb;
    [SerializeField] private GameObject changeButtons;
    [SerializeField] private GameObject closeButton;


    
    private void Awake()
    {
        _bgID = PlayerPrefs.GetInt("background", 0);
    }
    void Start()
    {
        SetItem("background");

    }

    public void SelectBackground(bool isForward)
    {
        if (isForward)
        {
            if (_bgID == backgrounds.Length - 1)
                _bgID = 0;
            else
                _bgID++;
        }
        else
        {
            if (_bgID == 0)
                _bgID = backgrounds.Length - 1;
            else
                _bgID--;
        }
        
        SetItem("background");
    }
    
    private void SetItem(string type)
    {
        switch (type)
        {
            case "background":
                foreach (var b in backgrounds)
                {
                    b.SetActive(false); 
                }

                PlayerPrefs.SetInt("background", _bgID);
                PlayerPrefs.Save();
                SoundController.Sound.CloseBtn ();
                backgrounds[_bgID].SetActive(true);
                break;
        }
    }

    public void OpenBackgroundsPopup()
    {
        foreach (var obj in disableOjb)
        {
            obj.SetActive(false); 
        }
        SoundController.Sound.CloseBtn ();
        changeButtons.SetActive(true);
        closeButton.SetActive(true);
    }
    
    public void CloseBackgroundsPopup()
    {
        foreach (var obj in disableOjb)
        {
            obj.SetActive(true); 
        }
        SoundController.Sound.CloseBtn ();
        changeButtons.SetActive(false);
        closeButton.SetActive(false);
    }
}
