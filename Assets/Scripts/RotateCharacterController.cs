using System;
using UnityEngine;

public class RotateCharacterController : MonoBehaviour
{
    [SerializeField] private float rotationSpeed = 100f;
    private bool _dragging;
    private Rigidbody rb;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnMouseDrag()
    {
        _dragging = true;
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            _dragging = false;
        }
    }

    private void LateUpdate()
    {
        if (!_dragging) return;
        var x = Input.GetAxis("Mouse X") * rotationSpeed * Time.fixedDeltaTime;
            
        rb.AddTorque(Vector3.down * x);
    }
}
